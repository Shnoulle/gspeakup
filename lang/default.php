<?

$lang = Array 
 ( 
  'PETITION_TITLE' => 'My first petition',
  'PETITION_NAME' => 'my first petition',
  'PETITION_BRAND' => '<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;gSpeakUp',
  'PETITION_HEADER' => 'This is my petition default header',
  'PETITION_BODY' => 'This is my petition default body.

<ul><li>I can even embed HTML!</li>
<li>And no limit, <b>XSS allowed</b>, woot!</li>
</ul>',
  'PETITION_ARGUMENT' => 'You should sign because otherwise, we <b>harm this kitten</b>!</p>
<p><img alt="Kitten" class="img-responsive" src="images/kitten.jpeg"/></p>
<p>Same here, HTML allowed.  Just understand that this is enclosed
within a &lt;p&gt; context, do not forget to close it if you open it.',

   );

?>
