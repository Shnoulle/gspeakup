<?php

// Language file for GPT
// Updated by  on 2014-09-29 21:49:51

$lang = Array ( 

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change confirmation' => 'Apstiprinājums par paraksta redzamības maiņu',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'A signature visibility change confirmation has been sent to email <tt>%s</tt>.' => 'Apstiprinājums par paraksta redzamības maiņu ir nosūtīts uz e-pastu <tt>%s</tt>.',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.' => 'Ja tuvākajā laikā nesaņemat apstiprinājuma e-pastu, lūdzu, pārbaudiet, vai e-pasts no <tt>%s</tt> nav mēstuļu mapē.',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change' => 'Paraksta redzamības maiņa',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Unable to find email <tt>%s</tt> in database.' => 'Datubāzē nav atrasts e-pasts <tt>%s</tt>',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you think this is an issue, please feel to contact us by email using the contact address.' => 'Ja šķiet, ka šī ir problēma, lūdzu, sazinieties ar mums pa e-pastu, izmantojot kontaktadresi',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Return to declaration page' => 'Atgriezties deklarācijas lapā',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'Signatures of the declaration' => 'Deklarācijas paraksti',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory from <em>%s</em>' => 'Parakstītājs, <em>%s</em>',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory' => 'Parakstītājs',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'No signature with this letter and country!' => 'Šajā valstī nav parakstu, kas sāktos ar šo burtu',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'How about helping us having more signatures by <a href="/post/Join-Us">spreading the word</a> or signing the declaration yourself, if not done already?' => 'Varbūt palīdzēsiet mums iegūt vairāk parakstu, <a href="/post/Join-Us">popularizējot</a> vai parakstot deklarāciju, ja tas vēl nav izdarīts?',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation done' => 'Paraksta atcelšana pabeigta',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation has been confirmed.' => 'Paraksta atcelšana ir apstiprināta.',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Enter email' => 'Ievadiet e-pasta adresi',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Validate my signature' => 'Validēt manu parakstu',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Why should I sign?' => 'Kāpēc parakstīt?',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Latest signatories' => 'Jaunākie parakstītāji',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'See all signatures' => 'Apskatīt visus parakstus',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatories' => 'parakstītāji',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatory' => 'parakstītājs',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'Error' => 'Kļūda',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'If you feel this is an error, please do not hesitate to contact us by email at contact[at]freesoftwarepact.eu.' => 'Ja šķiet, ka šī ir kļūda, lūdzu, sazinieties ar mums pa e-pastu: contact[at]freesoftwarepact.eu.',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Your signature is currently:' => 'Jūsu paraksts pašlaik ir:',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'public' => 'publisks',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'private' => 'privāts',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to private' => 'Izmainīt paraksta redzamību uz privātu',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to public' => 'Izmainīt paraksta redzamību uz publisku',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'or' => 'vai',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'return to declaration page' => 'atgriezties deklarācijas lapā',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'One last step to confirm your signature!' => 'Vēl pēdējais solis, lai apstiprinātu jūsu parakstu!',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'You have signed the declaration and we thank you a lot for your support!' => 'Deklarācija ir parakstīta, un mēs pateicamies par izrādīto atbalstu!',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'A confirmation mail has been sent to <tt>%s</tt>, please follow the confirmation link inside in order to confirm your signature.' => 'Apstiprināšanas e-pasts ir nosūtīts uz <tt>%s</tt>. Lūdzu, atveriet tajā atrodamo saiti, lai apstiprinātu savu parakstu.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I represent an organization' => 'Es pārstāvu organizāciju',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Email' => 'E-pasts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mandatory' => 'obligāts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization name' => 'Organizācijas nosaukums',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Description' => 'Apraksts',
	
	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization (very short) description' => 'Organizācijas apraksts (ļoti īsi)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Website' => 'Tīmekļa vietne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization website' => 'Organizācijas tīmekļa vietne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Country' => 'Valsts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Choose your country' => 'Izvēlieties valsti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'facultative' => 'neobligāts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European Union countries' => 'Eiropas Savienības valstis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Other countries' => 'Citas valstis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Confirm my signature' => 'Apstiprināt parakstu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I am a citizen' => 'Esmu pilsonis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Firstname' => 'Vārds',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Name' => 'Uzvārds',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Occupation' => 'Nodarbošanās',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'if applicable' => 'ja piemērojams',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European countries' => 'Eiropas valstis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Keep me informed of future campaigns' => 'Informēt mani par turpmākām kampaņām',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'My signature is private (hide my name)' => 'Mans paraksts ir privāts (nerādīt manu vārdu)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'No country matched:' => 'Valsts nav atrasta:',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'Change signature visibility' => 'Izmainīt paraksta redzamību',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation confirmation' => 'Paraksta atcelšanas apstiprinājums',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Cancellation confirmation has been sent to email <tt>%s</tt>.' => 'Paraksta atcelšanas apstiprinājums ir nosūtīts uz <tt>%s</tt>.',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation' => 'Paraksta atcelšana',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Please fill in your email address in order to authenticate your request.' => 'Lūdzu, ievadiet savu e-pasta adresi, lai autentificētu savu pieprasījumu.',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Cancel my signature' => 'Atcelt manu parakstu',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Adding your name to the list means that we show to the world how many of us there are. Don\'t hesitate to spread the word!' => 'Sava vārda pievienošana sarakstam parāda, cik daudzi mēs esam. Padodiet ziņu citiem!',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Do not hesitate to ask your friends and relative to sign this declaration or to share your signature using social networks.' => 'Droši aiciniet arī savus draugus un tuviniekus parakstīt šo deklarāciju vai publiskojiet savu parakstu sociālajos tīklos.',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share your signature!' => 'Publisko savu parakstu!',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on identi.ca/pump.io' => 'Rakstīt identi.ca/pump.io',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Text suggestion' => 'Teksta ierosinājums',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share!' => 'Nosūtīt!',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on twitter' => 'Rakstīt twitter',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'We nevertheless discourage the use of non-free Twitter in favor of the use of Pump.io. <a href=\'https://www.fsf.org/twitter\'>Read more</a> about the issues regarding Twitter on the Free Software Foundation website.' => 'Mēs tomēr dodam priekšroku brīvajam Pump.io; par problēmām ar Twitter <a href=\'https://www.fsf.org/twitter\'>lasiet vairāk</a> Free Software Foundation tīmekļa vietnē',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'English' => 'Angļu',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Dear %s' => '%s',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'To validate your signature, click on the following link :' => 'Lai apstiprinātu savu parakstu, sekojiet šai saitei:',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Note: some email programs would split the preceding URL in two lines.' => 'Ņemiet vērā, ka dažas e-pasta lasīšanas programmas saiti var sadalīt 2 rindās.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'To validate your request, click on the following link :' => 'Lai apstiprinātu pieprasījumu, sekojiet šai saitei:',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to write into %s.' => 'Neizdevās rakstīt %s.',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to connect to database.' => 'Neizdevās pieslēgties datubāzei.',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'<p><b>A SQL error has been triggered</b>.  Webmaster has been informed and will take actions soon.</p><p>You may want to reload page or retry later.</p>' => '<p><b>Notikusi kļūda, izpildot SQL skriptu</b>.  Par to ir paziņots administratoram, kurš drīz kļūdu izmeklēs.</p><p>Mēģiniet lapu ielādēt vēlreiz. Ja kļūda atkārtojas, tad mēģiniet vēlreiz vēlāk.</p>',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to find the signature and thus to validate it.' => 'Neizdevās atrast (un tāpēc arī validēt) parakstu.',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to send mail to email <tt>%s</tt>' => 'Neizdevās nosūtīt e-pastu uz adresi <tt>%s</tt>',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Invalid mail !' => 'Nepareiza e-pasta adrese!',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'You already have signed the declaration!' => 'Jūs jau parakstījāt šo deklarāciju!',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'If you want to receive a new validation mail, <a href="%s">click here</a>.' => 'Ja vēlaties saņemt jaunu validācijas e-pastu, <a href="%s">spiediet šeit</a>.',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to validate your signature!' => 'Neizdevās validēt jūsu parakstu!',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to cancel signature, wrong key.' => 'Neizdevās atcelt parakstu, jo norādīta nepareiza atslēga.',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Visibility changed' => 'Redzamība nomainīta',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to change visibility for signature, wrong key.' => 'Neizdevās mainīt paraksta redzamību, jo norādīta nepareiza atslēga.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'at' => 'Austrija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'be' => 'Beļģija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'bg' => 'Bulgārija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hr' => 'Horvātija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cy' => 'Kipra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cz' => 'Čehijas Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'dk' => 'Dānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ee' => 'Igaunija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fi' => 'Somija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fr' => 'Francija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'de' => 'Vācija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gr' => 'Grieķija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hu' => 'Ungārija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ie' => 'Īrija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'it' => 'Itālija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lv' => 'Latvija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lt' => 'Lietuva',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lu' => 'Luksemburga',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'mt' => 'Malta',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'nl' => 'Nīderlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pl' => 'Polija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pt' => 'Portugāle',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ro' => 'Rumānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'sk' => 'Slovākija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'si' => 'Slovēnija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'es' => 'Spānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'se' => 'Zviedrija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gb' => 'Lielbritānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'af' => 'Afganistāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ax' => 'Olande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'al' => 'Albānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dz' => 'Alžīrija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'as' => 'Amerikāņu Samoa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ad' => 'Andora',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ao' => 'Angola',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ai' => 'Angilja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aq' => 'Antarktika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ag' => 'Antigva un Barbuda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ar' => 'Argentīna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'am' => 'Armēnija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aw' => 'Aruba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'au' => 'Austrālija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'az' => 'Azerbaidžāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bs' => 'Bahamas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bh' => 'Bahreina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bd' => 'Bangladeša',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bb' => 'Barbadosa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'by' => 'Baltkrievija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bz' => 'Beliza',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bj' => 'Benina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bm' => 'Bermunda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bt' => 'Butāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bo' => 'Bolīvija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ba' => 'Bosnija un Hercegovina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bw' => 'Botsvana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bv' => 'Buvē Sala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'br' => 'Brazīlija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'io' => 'Indijas Okeāna Britu teritorija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bn' => 'Brunejas Darusalamas Valsts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bf' => 'Burkinafaso',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bi' => 'Burundi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kh' => 'Kambodža',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cm' => 'Kamerūna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ca' => 'Kanāda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cv' => 'Kaboverde',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ky' => 'Kaimanu salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cf' => 'Centrālāfrikas Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'td' => 'Čada',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cl' => 'Čīle',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cn' => 'Ķīna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cx' => 'Ziemsvētku sala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cc' => 'Kokosu (Kīlinga) salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'co' => 'Kolumbija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'km' => 'Komoras',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cg' => 'Kongo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cd' => 'Kongo Demokrātiskā Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ck' => 'Kuka salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cr' => 'Kostarika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ci' => 'Kotdivuāra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cu' => 'Kuba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dj' => 'Džibutija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dm' => 'Dominika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'do' => 'Dominikānas Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ec' => 'Ekvadora',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eg' => 'Ēģipte',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sv' => 'Salvadora',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gq' => 'Ekvatoriālā Gvineja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'er' => 'Eritreja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'et' => 'Etiopija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fk' => 'Folklenda (Malvinu) salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fo' => 'Fēru salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fj' => 'Fidži',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gf' => 'Gviāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pf' => 'Franču Polinēzija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tf' => 'Francijas Dienvidjūru Zemes',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ga' => 'Gabona',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gm' => 'Gambija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ge' => 'Gruzija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gh' => 'Gana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gi' => 'Gibraltārs',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gl' => 'Grenlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gd' => 'Grenāda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gp' => 'Gvadelupa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gu' => 'Guama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gt' => 'Gvatemala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gg' => 'Gērnsija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gn' => 'Gvineja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gw' => 'Gvineja-bissau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gy' => 'Gajāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ht' => 'Haiti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hm' => 'Hērda Sala un Makdonalda Salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'va' => 'Vatikāna Pilsētvalsts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hn' => 'Hondurasa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hk' => 'Honkonga',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'is' => 'Islande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'in' => 'Indija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'id' => 'Indonēzija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ir' => 'Irāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'iq' => 'Irāka',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'im' => 'Menas sala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'il' => 'Izraēla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jm' => 'Jamaika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jp' => 'Japāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'je' => 'Džersija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jo' => 'Jordānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kz' => 'Kazahstāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ke' => 'Kenija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ki' => 'Kiribati',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kp' => 'Ziemeļkoreja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kr' => 'Dienvidkoreja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kw' => 'Kuveita',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kg' => 'Kirgizstāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'la' => 'Laosa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lb' => 'Libāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ls' => 'Lesoto',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lr' => 'Libērija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ly' => 'Lībija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'li' => 'Lihtenšteina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mo' => 'Makao',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mk' => 'Maķedonijas Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mg' => 'Madagaskara',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mw' => 'Malāvija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'my' => 'Malaizija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mv' => 'Maldīvija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ml' => 'Mali',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mh' => 'Māršala salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mq' => 'Martinika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mr' => 'Mauritānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mu' => 'Maurīcija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'yt' => 'Majota',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mx' => 'Meksika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fm' => 'Mikronēzijas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'md' => 'Moldova',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mc' => 'Monako',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mn' => 'Mongolija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'me' => 'Melnkalne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ms' => 'Montserrata',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ma' => 'Maroka',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mz' => 'Mozambika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mm' => 'Mjanma',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'na' => 'Namībija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nr' => 'Nauru',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'np' => 'Nepāla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'an' => 'Nīderlande Antilles',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nc' => 'Jaunkaledonija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nz' => 'Jaunzēlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ni' => 'Nikaragva',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ne' => 'Nigēra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ng' => 'Nigērija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nu' => 'Niue',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nf' => 'Norfolkas Sala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mp' => 'Ziemeļu Marianas Salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'no' => 'Norvēģija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'om' => 'Omāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pk' => 'Pakistāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pw' => 'Palau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ps' => 'Palestīna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pa' => 'Panama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pg' => 'Papua-Jaungvineja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'py' => 'Paragvaja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pe' => 'Peru',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ph' => 'Filipīnas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pn' => 'Pitkērna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pr' => 'Puertoriko',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'qa' => 'Katara',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	're' => 'Reinjona',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ru' => 'Krievijas Federācija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rw' => 'Ruanda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sh' => 'Sv. Helēnas sala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kn' => 'Sentkitsa un Nevisa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lc' => 'Sentlūsija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pm' => 'Senpjēra un Mikelona',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vc' => 'Sentvinsenta un Grenadīnas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ws' => 'Samoa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sm' => 'Sanmarīno',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'st' => 'Santome un Prinsipi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sa' => 'Saūda Arābija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sn' => 'Senegāla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rs' => 'Serbija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sc' => 'Seišelas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sl' => 'Sjerraleone',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sg' => 'Singapūra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sb' => 'Zālamana salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'so' => 'Somālija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'za' => 'Dienvidāfrika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gs' => 'Indijas Okeāna Britu Teritorija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lk' => 'Šrilanka',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sd' => 'Sudāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sr' => 'Surinama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sj' => 'Svalbāra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sz' => 'Svazilenda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ch' => 'Šveice',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sy' => 'Sīrija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tw' => 'Taivāna, Ķīnas Republika',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tj' => 'Tadžikistāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tz' => 'Tanzānija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'th' => 'Taizeme',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tl' => 'Austrumtimora',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tg' => 'Togo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tk' => 'Tokelau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'to' => 'Tonga',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tt' => 'Trinidāda un Tobāgo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tn' => 'Tunisija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tr' => 'Turcija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tm' => 'Turkmenistāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tc' => 'Tērksu un Kaikosu salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tv' => 'Tuvalu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ug' => 'Uganda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ua' => 'Ukraina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ae' => 'Apvienotie Arābu Emirāti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'us' => 'Amerikas Savienotās Valstis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'um' => 'ASV Mazās Aizjūras Salas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uy' => 'Urugvaja',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uz' => 'Uzbekistāna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vu' => 'Vanuatu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	've' => 'Venecuēla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vn' => 'Vjetnama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vg' => 'Virdžīnas Salas, Britu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vi' => 'ASV Virdžīnas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'wf' => 'Volisa un Futuna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eh' => 'Rietumsahāra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ye' => 'Jemena',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zm' => 'Zambija',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zw' => 'Zimbabve',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'PETITION_BRAND' => '<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;gSpeakup',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Language' => 'Valoda',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'French' => 'Franču',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Latvian' => 'Latviešu',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'My signature' => 'Mans paraksts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_HEADER' => 'Šis ir manas petīcijas noklusētais virsraksts',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_BODY' => 'Šis ir manas petīcijas noklusētais saturs.

<ul><li>Es pat varu ievietot HTML!</li>
<li>Un nekādu ierobežojumu, <b>pat XSS atļauts</b>, juhū!</li>
</ul>',
  
	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'<b>Note:</b> We will not publish or share any of your information with any party outside us.' => '<b>Atceries:</b> Mēs nepublicēsim un nenodosim nevienai trešajai pusei šo informāciju.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'PETITION_TITLE' => 'Mana pirmā petīcija',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Thanks for your signature and your support!' => 'Paldies par parakstu un atbalstu!',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'I have signed %s!  What about YOU?  %s' => 'Es parakstīju "%s". Kā ar TEVI?',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_ARGUMENT' => 'Tev jāparaksta šī petīcija, citādi <b>kaķēniem tiek nodarītas sāpes</b>!</p>
<p><img class="img-responsive" src="images/kitten.jpeg" alt="Kitten"/></p>
<p>Šeit tāpat HTML ir atļauts.  Tikai atcerieties, ka šis tiks iekļauts starp paragrāfa birkām &lt;p&gt;, tāpēc, ja pievienojat savas paragrāfa birkas, tad atcerieties tās aizvērt.',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Signatories map' => 'Parakstītāju karte',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'%s signatures out of a %d goal' => '%s paraksti no vajadzīgajiem %d',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to change your signature visibility (either set it public or private).' => 'Jūs esat parakstījis deklarāciju \'<b>%s</b>\' un vēlaties mainīt sava paraksta redzamību (nomainīt to vai nu uz publisku vai privātu).',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to cancel your signature.' => 'Jūs esat parakstījis deklarāciju \'<b>%s</b>\' un vēlaties atsaukt savu parakstu.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'you receive this email as you just signed the %s declaration.  We require that you confirm your signature in order to avoid false signatures.' => 'Jūs saņemat šo e-pastu, jo parakstījāt deklarāciju \'%s\'. Lūdzu apstipriniet savu parakstu.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl
	'you receive this email as you asked to cancel your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => 'Jūs saņemat šo e-pastu, jo pieprasījāt sava paraksta atcelšanu deklarācijai \'%s\'. Lūdzu apstipriniet sava paraksta atcelšanu.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'you receive this email as you asked to change visibility for your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => 'Jūs saņemat šo e-pastu, jo pieprasījāt nomainīt redzamību parakstītajai deklarācijai \'%s\'. Lūdzu apstipriniet sava paraksta redzamības maiņu.',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php, /home/benj/public_html/declaration//include/utils.php
	'PETITION_NAME' => 'mana pirmā petīcija',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Missing field "%s"' => 'Nav aizpildīts lauks "%s"',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature confirmation for the %s declaration' => 'Paraksta apstiprinājums deklarācijai "%s"',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature cancellation for the %s declaration' => 'Paraksta atcelšana deklarācijai "%s"',

	);

?>
