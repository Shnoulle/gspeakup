<?php

// Language file for GPT
// Updated by  on 2014-09-29 21:13:41

$lang = Array ( 

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'PETITION_TITLE' => 'Ma première pétition',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php, /home/benj/public_html/declaration//include/utils.php
	'PETITION_NAME' => 'ma première pétition',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'PETITION_BRAND' => '<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;gSpeakUp',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_HEADER' => 'Titre par défaut de ma pétition',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_BODY' => 'Corps par défaut de ma pétition.
<ul><li>Je peux même y mettre du HTML !</li>
<li>Pas de limite, <b>le XSS est autorisé</b>, super !</li>
</ul>',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_ARGUMENT' => 'Parce que sinon, on tue un chaton !</p>
<p><img class="img-responsive" src="images/kitten.jpeg" alt="Kitten"/></p>
<p>Pareil, l\'HTML est autorisé.  Simplement, pensez qu\'il est inclus dans un 
contexte &lt;p&gt;, ne pas oublier de fermer les blocs.',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Signatories map' => 'Carte des signataires',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'My signature' => 'Ma signature',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Language' => 'Langue',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Email' => 'Courriel',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Invalid mail !' => 'Mail invalide !',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Validate my signature' => 'Valider ma signature',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'See all signatures' => 'Voir toutes les signatures',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Keep me informed of future campaigns' => 'Me tenir au courant des campagnes futures',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'My signature is private (hide my name)' => 'Ma signature est privée (cacher mon nom)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'<b>Note:</b> We will not publish or share any of your information with any party outside us.' => '<b>Note:</b> nous ne publierons ni ne partagerons vos informations avec qui que ce soit en dehors de notre organisation.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I am a citizen' => 'Je suis un citoyen',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Occupation' => 'Profession',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'if applicable' => 'si applicable',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I represent an organization' => 'Je représente une organisation',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization name' => 'Nom de l\'organisation',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization (very short) description' => 'Description (très courte) de l\'organisation',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization website' => 'Site web de l\'organisation',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Website' => 'Site web',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'A confirmation mail has been sent to <tt>%s</tt>, please follow the confirmation link inside in order to confirm your signature.' => 'Un mail de confirmation a été envoyé à <tt>%s</tt>, veuillez suivre le lien de confirmation qui s\'y trouve pour confirmer votre signature.',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to change visibility for signature, wrong key.' => 'Impossible de changer la visibilité de la signature, mauvaise clef',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change confirmation' => 'Confirmation de changement de visibilité de signature',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.' => 'Si vous ne recevez pas de mail de confirmation sous peu, veuillez vérifier votre boîte à spams à la recherche d\'un mail en provenance de <tt>%s</tt>.',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'You already have signed the declaration!' => 'Vous avez déjà signé la déclaration !',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'If you want to receive a new validation mail, <a href="%s">click here</a>.' => 'Si vous voulez recevoir un nouveau mail de confirmation, veuillez <a href="%s">cliquez ici</a>.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Confirm my signature' => 'Confirmer ma signature',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature confirmation for the %s declaration' => 'Confirmation de votre signature de la déclaration %s',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature cancellation for the %s declaration' => 'Annulation de votre signature de la déclaration %s',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to validate your signature!' => 'Impossible de valider votre signature !',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to find the signature and thus to validate it.' => 'Impossible de valider votre signature et ainsi de la valider.',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'One last step to confirm your signature!' => 'Dernière étape pour confirmer votre signature&nbsp;!',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'You have signed the declaration and we thank you a lot for your support!' => 'Vous avez signé la déclaration et nous vous remercions pour votre soutien&nbsp;!',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Return to declaration page' => 'Retourner à la page de la déclaration',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'Signatures of the declaration' => 'Signataires de la déclaration',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'No signature with this letter and country!' => 'Pas de signature pour le moment pour ce pays et cette lettre&nbsp;!',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'How about helping us having more signatures by <a href="/post/Join-Us">spreading the word</a> or signing the declaration yourself, if not done already?' => 'Et si vous nous aidiez à avoir plus de signatures en <a href="/post/Join-Us">faisant passer le mot</a> et en signant la déclaration vous-même si ce n\'est pas déjà fait&nbsp;?',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to cancel your signature.' => 'Vous avez signé la déclaration «&nbsp;<b>%s</b>&nbsp;» et voulez annuler votre signature.',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Please fill in your email address in order to authenticate your request.' => 'Veuillez entrer votre adresse courriel ci-dessous pour authentifier votre demande.',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Enter email' => 'Entrer mon courriel',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'or' => 'ou',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'return to declaration page' => 'retourner à la page de la déclaration',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Cancel my signature' => 'Annuler ma signature',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation' => 'Annulation de signature',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation confirmation' => 'Confirmation d\'annulation de signature',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Unable to find email <tt>%s</tt> in database.' => 'Impossible de trouver le courriel <tt>%s</tt> dans notre base',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you think this is an issue, please feel to contact us by email using the contact address.' => 'Si vous pensez que c\'est une erreur, veuillez nous contacter par courriel en utilisant l\'adresse de contact',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'If you feel this is an error, please do not hesitate to contact us by email at contact[at]freesoftwarepact.eu.' => 'Si vous pensez que c\'est une erreur, veuillez nous contacter par courriel en utilisant l\'adresse contact[at]freesoftwarepact.eu',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to send mail to email <tt>%s</tt>' => 'Impossible d\'envoyer un courriel à l\'adresse <tt>%s</tt>',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to cancel signature, wrong key.' => 'Impossible d\'annuler vote signature, mauvaise clef.',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Cancellation confirmation has been sent to email <tt>%s</tt>.' => 'Une confirmation d\'annulation a été envoyée au courriel <tt>%s</tt>.',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'A signature visibility change confirmation has been sent to email <tt>%s</tt>.' => 'Une confirmation de modification de visibilité a été envoyée au courriel <tt>%s</tt>.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'you receive this email as you just signed the %s declaration.  We require that you confirm your signature in order to avoid false signatures.' => 'vous recevez ce courriel car vous venez tout juste de signer la déclaration %s.  Une vérification de votre courriel est nécessaire pour éviter les fausses signatures.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl
	'you receive this email as you asked to cancel your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => 'vous recevez ce courriel car vous venez tout juste de demander d\'annuler votre signature de la déclaration %s.  Une vérification de votre courriel est nécessaire pour éviter les fausses demandes.',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'Change signature visibility' => 'Modifier la visibilité de ma signature',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to change your signature visibility (either set it public or private).' => 'Vous avez signé la déclaration «&nbsp;<b>%s</b>&nbsp;» et voulez changer la visibilité de votre signature (pour la rendre publique ou privée).',

	// In files /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'you receive this email as you asked to change visibility for your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => 'vous recevez ce courriel car vous venez tout juste de demander à changer la visibilité de votre signature de la déclaration %s.  Une vérification de votre courriel est nécessaire pour éviter les fausses demandes.',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'To validate your request, click on the following link :' => 'Pour valider votre demande, validez le lien suivant :',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'To validate your signature, click on the following link :' => 'Pour valider votre signature, validez le lien suivant :',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Dear %s' => 'Cher(e) %s',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Thanks for your signature and your support!' => 'Merci pour votre signature et votre soutien !',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'I have signed %s!  What about YOU?  %s' => 'J\'ai signé %s, et VOUS ?  %s',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share your signature!' => 'Partagez votre signature !',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Why should I sign?' => 'Pourquoi signer ?',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change' => 'Changement de visibilité de signature',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Your signature is currently:' => 'Votre signature est actuellement:',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to private' => 'Rendre ma signature privée',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to public' => 'Rendre ma signature publique',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'public' => 'publique',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'private' => 'privée',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Visibility changed' => 'Visibilité modifiée',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on twitter' => 'Partager sur twitter',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on identi.ca/pump.io' => 'Partager sur identi.ca/pump.io',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Text suggestion' => 'Suggestion de texte',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'We nevertheless discourage the use of non-free Twitter in favor of the use of Pump.io. <a href=\'https://www.fsf.org/twitter\'>Read more</a> about the issues regarding Twitter on the Free Software Foundation website.' => 'Nous décourageons cependant l’utilisation du service non-libre Twitter en faveur de Pump.io. <a href=\'https://www.fsf.org/twitter\'>En lire plus</a> sur le site de la Free Software Foundation',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Do not hesitate to ask your friends and relative to sign this declaration or to share your signature using social networks.' => 'N\'hésitez pas à demander à vos amis ou à votre famille de signer la déclaration ou à partager votre signature sur les réseaux sociaux',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'Error' => 'Erreur',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to connect to database.' => 'Impossible de se connecter à la base de données.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Name' => 'Nom',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Country' => 'Pays',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Description' => 'Description',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatory' => 'signataire',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatories' => 'signataires',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Note: some email programs would split the preceding URL in two lines.' => 'Note: certains logiciels de courriel peuvent séparer l\'URL précédente 
en deux lignes.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Firstname' => 'Prénom',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'<p><b>A SQL error has been triggered</b>.  Webmaster has been informed and will take actions soon.</p><p>You may want to reload page or retry later.</p>' => '<p><b>Une erreur SQL a été produite</b>.  Le webmaster a été informé et produira les actions nécessaires dès que possible.</p><p>Vous pouvez recharger la page ou réessayer plus tard.</p>',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Choose your country' => 'Choisir son pays',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Other countries' => 'Autres pays',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European Union countries' => 'Pays Européens',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory' => 'Un(e) signataire',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory from <em>%s</em>' => 'Un(e) signataire de: <em>%s</em>',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mandatory' => 'obligatoire',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'facultative' => 'facultatif',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Latest signatories' => 'Derniers signataires',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'%s signatures out of a %d goal' => 'Déjà <b>%d signatures</b> sur un objectif de <b>%d</b> signatures',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ad' => 'Andorre',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ae' => 'Émirats arabes unis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'af' => 'Afghanistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ag' => 'Antigua-et-Barbuda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ai' => 'Anguilla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'al' => 'Albanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'am' => 'Arménie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ao' => 'Angola',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aq' => 'Antarctique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ar' => 'Argentine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'as' => 'Samoa américaines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'at' => 'Autriche',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'au' => 'Australie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aw' => 'Aruba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ax' => 'Îles Åland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'az' => 'Azerbaïdjan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ba' => 'Bosnie-Herzégovine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bb' => 'Barbade',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bd' => 'Bangladesh',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'be' => 'Belgique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bf' => 'Burkina Faso',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'bg' => 'Bulgarie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bh' => 'Bahreïn',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bi' => 'Burundi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bj' => 'Bénin',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bm' => 'Bermudes',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bn' => 'Brunei',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bo' => 'Bolivie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'br' => 'Brésil',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bs' => 'Bahamas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bt' => 'Bhoutan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bv' => 'Île Bouvet',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bw' => 'Botswana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'by' => 'Biélorussie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bz' => 'Belize',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ca' => 'Canada',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cc' => 'Îles Cocos',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cd' => 'République démocratique du Congo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cf' => 'République centrafricaine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cg' => 'République du Congo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ch' => 'Suisse',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ci' => 'Côte d\'Ivoire',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ck' => 'Îles Cook',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cl' => 'Chili',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cm' => 'Cameroun',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cn' => 'Chine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'co' => 'Colombie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cr' => 'Costa Rica',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cu' => 'Cuba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cv' => 'Cap-Vert',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cx' => 'Île Christmas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cy' => 'Chypre',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cz' => 'République tchèque',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'de' => 'Allemagne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dj' => 'Djibouti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'dk' => 'Danemark',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dm' => 'Dominique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'do' => 'République dominicaine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dz' => 'Algérie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ec' => 'Équateur',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ee' => 'Estonie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eg' => 'Égypte',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eh' => 'République arabe sahraouie démocratique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'er' => 'Érythrée',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'es' => 'Espagne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'et' => 'Éthiopie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fi' => 'Finlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fj' => 'Fidji',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fk' => 'Malouines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fm' => 'Micronésie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fo' => 'Îles Féroé',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fr' => 'France',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ga' => 'Gabon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gb' => 'Royaume-Uni',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gd' => 'Grenade',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ge' => 'Géorgie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gf' => 'Guyane',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gg' => 'Guernesey',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gh' => 'Ghana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gi' => 'Gibraltar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gl' => 'Groenland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gm' => 'Gambie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gn' => 'Guinée',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gp' => 'Guadeloupe',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gq' => 'Guinée équatoriale',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gr' => 'Grèce',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gs' => 'Géorgie du Sud-et-les Îles Sandwich du Sud',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gt' => 'Guatemala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gu' => 'Guam',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gw' => 'Guinée-Bissau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gy' => 'Guyana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hk' => 'Hong Kong',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hm' => 'Îles Heard-et-MacDonald',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hn' => 'Honduras',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hr' => 'Croatie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ht' => 'Haïti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hu' => 'Hongrie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'id' => 'Indonésie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ie' => 'Irlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'il' => 'Israël',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'im' => 'Île de Man',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'in' => 'Inde',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'io' => 'Territoire britannique de l\'océan Indien',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'iq' => 'Irak',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ir' => 'Iran',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'is' => 'Islande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'it' => 'Italie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'je' => 'Jersey',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jm' => 'Jamaïque',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jo' => 'Jordanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jp' => 'Japon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ke' => 'Kenya',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kg' => 'Kirghizistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kh' => 'Cambodge',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ki' => 'Kiribati',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'km' => 'Comores',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kn' => 'Saint-Christophe-et-Niévès',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kp' => 'Corée du Nord',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kr' => 'Corée du Sud',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kw' => 'Koweït',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ky' => 'Îles Caïmans',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kz' => 'Kazakhstan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'la' => 'Laos',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lb' => 'Liban',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lc' => 'Sainte-Lucie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'li' => 'Liechtenstein',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lk' => 'Sri Lanka',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lr' => 'Liberia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ls' => 'Lesotho',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lt' => 'Lituanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lu' => 'Luxembourg',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lv' => 'Lettonie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ly' => 'Libye',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ma' => 'Maroc',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mc' => 'Monaco',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'md' => 'Moldavie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'me' => 'Monténégro',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mg' => 'Madagascar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mh' => 'Îles Marshall',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mk' => 'Macédoine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ml' => 'Mali',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mm' => 'Birmanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mn' => 'Mongolie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mo' => 'Macao',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mp' => 'Îles Mariannes du Nord',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mq' => 'Martinique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mr' => 'Mauritanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ms' => 'Montserrat',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'mt' => 'Malte',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mu' => 'Maurice',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mv' => 'Maldives',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mw' => 'Malawi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mx' => 'Mexique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'my' => 'Malaisie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mz' => 'Mozambique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'na' => 'Namibie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nc' => 'Nouvelle-Calédonie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ne' => 'Niger',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nf' => 'Île Norfolk',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ng' => 'Nigeria',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ni' => 'Nicaragua',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'nl' => 'Pays-Bas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'no' => 'Norvège',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'np' => 'Népal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nr' => 'Nauru',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nu' => 'Niue',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nz' => 'Nouvelle-Zélande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'om' => 'Oman',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pa' => 'Panama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pe' => 'Pérou',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pf' => 'Polynésie française',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pg' => 'Papouasie-Nouvelle-Guinée',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ph' => 'Philippines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pk' => 'Pakistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pl' => 'Pologne',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pm' => 'Saint-Pierre-et-Miquelon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pn' => 'Îles Pitcairn',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pr' => 'Porto Rico',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ps' => 'Palestine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pt' => 'Portugal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pw' => 'Palaos',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'py' => 'Paraguay',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'qa' => 'Qatar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	're' => 'La Réunion',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ro' => 'Roumanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rs' => 'Serbie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ru' => 'Russie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rw' => 'Rwanda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sa' => 'Arabie saoudite',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sb' => 'Salomon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sc' => 'Seychelles',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sd' => 'Soudan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'se' => 'Suède',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sg' => 'Singapour',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sh' => 'Sainte-Hélène, Ascension et Tristan da Cunha|',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'si' => 'Slovénie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sj' => 'Svalbard et ile Jan Mayen',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'sk' => 'Slovaquie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sl' => 'Sierra Leone',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sm' => 'Saint-Marin',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sn' => 'Sénégal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'so' => 'Somalie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sr' => 'Suriname',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'st' => 'Sao Tomé-et-Principe',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sv' => 'Salvador',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sy' => 'Syrie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sz' => 'Swaziland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tc' => 'Îles Turques-et-Caïques',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'td' => 'Tchad',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tf' => 'Terres australes et antarctiques françaises',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tg' => 'Togo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'th' => 'Thaïlande',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tj' => 'Tadjikistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tk' => 'Tokelau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tl' => 'Timor oriental',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tm' => 'Turkménistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tn' => 'Tunisie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'to' => 'Tonga',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tr' => 'Turquie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tt' => 'Trinité-et-Tobago',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tv' => 'Tuvalu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tw' => 'Taïwan / (République de Chine (Taïwan))',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tz' => 'Tanzanie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ua' => 'Ukraine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ug' => 'Ouganda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'um' => 'Îles mineures éloignées des États-Unis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'us' => 'États-Unis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uy' => 'Uruguay',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uz' => 'Ouzbékistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'va' => 'Saint-Siège (État de la Cité du Vatican)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vc' => 'Saint-Vincent-et-les Grenadines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	've' => 'Venezuela',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vg' => 'Îles Vierges britanniques',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vi' => 'Îles Vierges des États-Unis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vn' => 'Viêt Nam',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vu' => 'Vanuatu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'wf' => 'Wallis-et-Futuna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ws' => 'Samoa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ye' => 'Yémen',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'yt' => 'Mayotte',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'za' => 'Afrique du Sud',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zm' => 'Zambie',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zw' => 'Zimbabwe',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'English' => 'Anglais',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'French' => 'Français',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Latvian' => 'Letton',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'an' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European countries' => 'Pays Européens',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'No country matched:' => 'Pas de pays trouvé',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Adding your name to the list means that we show to the world how many of us there are. Don\'t hesitate to spread the word!' => 'Ajouter votre nom à cette liste signifie que nous voulons montrer au monde combien nous sommes.  N\'hésitez pas à faire passer le mot !',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share!' => 'Partager !',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation done' => 'Annulation de la signature effectuée',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation has been confirmed.' => 'L\'annulation de la signature a été confirmée.',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to write into %s.' => 'Impossible d\'écrire dans %s.',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Missing field "%s"' => 'Champ "%s" manquant',

	);

?>
