<?php

// Language file for GPT
// Updated by  on 2014-09-30 14:16:01

$lang = Array ( 

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Name' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Email' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Description' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'Error' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Website' => '',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to write into %s.' => '',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'Unable to connect to database.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Country' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Firstname' => '',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//include/utils.php
	'<p><b>A SQL error has been triggered</b>.  Webmaster has been informed and will take actions soon.</p><p>You may want to reload page or retry later.</p>' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'at' => 'Austria',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'be' => 'Belgium',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'bg' => 'Bulgaria',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hr' => 'Croatia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cy' => 'Cyprus',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'cz' => 'Czech Republic',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'dk' => 'Denmark',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ee' => 'Estonia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fi' => 'Finland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'fr' => 'France',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'de' => 'Germany',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gr' => 'Greece',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'hu' => 'Hungary',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ie' => 'Ireland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'it' => 'Italy',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lv' => 'Latvia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lt' => 'Lithuania',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'lu' => 'Luxembourg',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'mt' => 'Malta',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'nl' => 'Netherlands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pl' => 'Poland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'pt' => 'Portugal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'ro' => 'Romania',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'sk' => 'Slovakia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'si' => 'Slovenia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'es' => 'Spain',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'se' => 'Sweden',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'gb' => 'United Kingdom',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'af' => 'Afghanistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ax' => 'Åland Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'al' => 'Albania',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dz' => 'Algeria',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'as' => 'American Samoa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ad' => 'Andorra',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ao' => 'Angola',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ai' => 'Anguilla',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aq' => 'Antarctica',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ag' => 'Antigua and Barbuda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ar' => 'Argentina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'am' => 'Armenia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'aw' => 'Aruba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'au' => 'Australia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'az' => 'Azerbaijan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bs' => 'Bahamas',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bh' => 'Bahrain',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bd' => 'Bangladesh',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bb' => 'Barbados',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'by' => 'Belarus',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bz' => 'Belize',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bj' => 'Benin',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bm' => 'Bermuda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bt' => 'Bhutan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bo' => 'Bolivia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ba' => 'Bosnia and Herzegovina',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bw' => 'Botswana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bv' => 'Bouvet Island',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'br' => 'Brazil',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'io' => 'British Indian Ocean Territory',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bn' => 'Brunei Darussalam',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bf' => 'Burkina Faso',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'bi' => 'Burundi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kh' => 'Cambodia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cm' => 'Cameroon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ca' => 'Canada',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cv' => 'Cape Verde',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ky' => 'Cayman Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cf' => 'Central African Republic',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'td' => 'Chad',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cl' => 'Chile',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cn' => 'China',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cx' => 'Christmas Island',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cc' => 'Cocos (Keeling) Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'co' => 'Colombia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'km' => 'Comoros',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cg' => 'Congo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cd' => 'Congo, The Democratic Republic of The',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ck' => 'Cook Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cr' => 'Costa Rica',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ci' => 'Cote D\'ivoire',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'cu' => 'Cuba',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dj' => 'Djibouti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'dm' => 'Dominica',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'do' => 'Dominican Republic',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ec' => 'Ecuador',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eg' => 'Egypt',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sv' => 'El Salvador',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gq' => 'Equatorial Guinea',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'er' => 'Eritrea',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'et' => 'Ethiopia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fk' => 'Falkland Islands (Malvinas)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fo' => 'Faroe Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fj' => 'Fiji',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gf' => 'French Guiana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pf' => 'French Polynesia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tf' => 'French Southern Territories',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ga' => 'Gabon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gm' => 'Gambia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ge' => 'Georgia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gh' => 'Ghana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gi' => 'Gibraltar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gl' => 'Greenland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gd' => 'Grenada',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gp' => 'Guadeloupe',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gu' => 'Guam',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gt' => 'Guatemala',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gg' => 'Guernsey',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gn' => 'Guinea',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gw' => 'Guinea-bissau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gy' => 'Guyana',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ht' => 'Haiti',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hm' => 'Heard Island and Mcdonald Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'va' => 'Holy See (Vatican City State)',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hn' => 'Honduras',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'hk' => 'Hong Kong',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'is' => 'Iceland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'in' => 'India',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'id' => 'Indonesia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ir' => 'Iran, Islamic Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'iq' => 'Iraq',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'im' => 'Isle of Man',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'il' => 'Israel',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jm' => 'Jamaica',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jp' => 'Japan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'je' => 'Jersey',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'jo' => 'Jordan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kz' => 'Kazakhstan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ke' => 'Kenya',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ki' => 'Kiribati',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kp' => 'Korea, Democratic People\'s Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kr' => 'Korea, Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kw' => 'Kuwait',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kg' => 'Kyrgyzstan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'la' => 'Lao People\'s Democratic Republic',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lb' => 'Lebanon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ls' => 'Lesotho',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lr' => 'Liberia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ly' => 'Libyan Arab Jamahiriya',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'li' => 'Liechtenstein',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mo' => 'Macao',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mk' => 'Macedonia, The Former Yugoslav Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mg' => 'Madagascar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mw' => 'Malawi',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'my' => 'Malaysia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mv' => 'Maldives',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ml' => 'Mali',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mh' => 'Marshall Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mq' => 'Martinique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mr' => 'Mauritania',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mu' => 'Mauritius',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'yt' => 'Mayotte',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mx' => 'Mexico',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'fm' => 'Micronesia, Federated States of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'md' => 'Moldova, Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mc' => 'Monaco',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mn' => 'Mongolia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'me' => 'Montenegro',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ms' => 'Montserrat',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ma' => 'Morocco',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mz' => 'Mozambique',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mm' => 'Myanmar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'na' => 'Namibia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nr' => 'Nauru',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'np' => 'Nepal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'an' => 'Netherlands Antilles',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nc' => 'New Caledonia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nz' => 'New Zealand',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ni' => 'Nicaragua',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ne' => 'Niger',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ng' => 'Nigeria',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nu' => 'Niue',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'nf' => 'Norfolk Island',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mp' => 'Northern Mariana Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'no' => 'Norway',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'om' => 'Oman',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pk' => 'Pakistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pw' => 'Palau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ps' => 'Palestinian Territory, Occupied',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pa' => 'Panama',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pg' => 'Papua New Guinea',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'py' => 'Paraguay',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pe' => 'Peru',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ph' => 'Philippines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pn' => 'Pitcairn',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pr' => 'Puerto Rico',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'qa' => 'Qatar',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	're' => 'Reunion',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ru' => 'Russian Federation',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rw' => 'Rwanda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sh' => 'Saint Helena',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'kn' => 'Saint Kitts and Nevis',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lc' => 'Saint Lucia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'pm' => 'Saint Pierre and Miquelon',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vc' => 'Saint Vincent and The Grenadines',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ws' => 'Samoa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sm' => 'San Marino',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'st' => 'Sao Tome and Principe',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sa' => 'Saudi Arabia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sn' => 'Senegal',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'rs' => 'Serbia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sc' => 'Seychelles',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sl' => 'Sierra Leone',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sg' => 'Singapore',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sb' => 'Solomon Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'so' => 'Somalia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'za' => 'South Africa',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'gs' => 'South Georgia and The South Sandwich Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'lk' => 'Sri Lanka',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sd' => 'Sudan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sr' => 'Suriname',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sj' => 'Svalbard and Jan Mayen',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sz' => 'Swaziland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ch' => 'Switzerland',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'sy' => 'Syrian Arab Republic',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tw' => 'Taiwan, Province of China',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tj' => 'Tajikistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tz' => 'Tanzania, United Republic of',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'th' => 'Thailand',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tl' => 'Timor-leste',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tg' => 'Togo',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tk' => 'Tokelau',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'to' => 'Tonga',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tt' => 'Trinidad and Tobago',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tn' => 'Tunisia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tr' => 'Turkey',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tm' => 'Turkmenistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tc' => 'Turks and Caicos Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'tv' => 'Tuvalu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ug' => 'Uganda',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ua' => 'Ukraine',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ae' => 'United Arab Emirates',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'us' => 'United States',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'um' => 'United States Minor Outlying Islands',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uy' => 'Uruguay',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'uz' => 'Uzbekistan',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vu' => 'Vanuatu',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	've' => 'Venezuela',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vn' => 'Viet Nam',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vg' => 'Virgin Islands, British',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'vi' => 'Virgin Islands, U.S.',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'wf' => 'Wallis and Futuna',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'eh' => 'Western Sahara',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'ye' => 'Yemen',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zm' => 'Zambia',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'zw' => 'Zimbabwe',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'PETITION_BRAND' => '<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;gSpeakUp',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Language' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'English' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'French' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'Latvian' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl
	'My signature' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'Change signature visibility' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Cancel my signature' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/header.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_HEADER' => 'Default title',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_BODY' => 'Default body for this petition.
<ul><li>You can even use HTML!</li>
<li>No limit, <b>XSS galore</b>!</li>
</ul>',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I represent an organization' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'mandatory' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization name' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization (very short) description' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Organization website' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Choose your country' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'facultative' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European Union countries' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Other countries' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Confirm my signature' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'I am a citizen' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Occupation' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'if applicable' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'European countries' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'Keep me informed of future campaigns' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'My signature is private (hide my name)' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'<b>Note:</b> We will not publish or share any of your information with any party outside us.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl
	'No country matched:' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/sign.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'PETITION_TITLE' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Your signature is currently:' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'public' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'private' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to private' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl
	'Set my signature to public' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'or' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-validate.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'return to declaration page' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'Signature visibility change confirmation' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl
	'A signature visibility change confirmation has been sent to email <tt>%s</tt>.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Unable to find email <tt>%s</tt> in database.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'If you think this is an issue, please feel to contact us by email using the contact address.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/validate.tpl, /home/benj/public_html/declaration//templates/smarty/croak.tpl, /home/benj/public_html/declaration//templates/smarty/confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl, /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Return to declaration page' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'Signatures of the declaration' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory from <em>%s</em>' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl, /home/benj/public_html/declaration//templates/smarty/index.tpl
	'A signatory' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'No signature with this letter and country!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/signatures.tpl
	'How about helping us having more signatures by <a href="/post/Join-Us">spreading the word</a> or signing the declaration yourself, if not done already?' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Thanks for your signature and your support!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Adding your name to the list means that we show to the world how many of us there are. Don\'t hesitate to spread the word!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Do not hesitate to ask your friends and relative to sign this declaration or to share your signature using social networks.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share your signature!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'I have signed %s!  What about YOU?  %s' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on identi.ca/pump.io' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Text suggestion' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'Share on twitter' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/validate.tpl
	'We nevertheless discourage the use of non-free Twitter in favor of the use of Pump.io. <a href=\'https://www.fsf.org/twitter\'>Read more</a> about the issues regarding Twitter on the Free Software Foundation website.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/croak.tpl
	'If you feel this is an error, please do not hesitate to contact us by email at contact[at]freesoftwarepact.eu.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Why should I sign?' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'PETITION_ARGUMENT' => 'Otherwise, we harm the kitten!</p>
<p><img class="img-responsive" src="images/kitten.jpeg" alt="Kitten"/></p>
<p>HTML is allowed.  Do not forget this text is enclosed between &lt;p&gt; context so don\'t close blocks.',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Signatories map' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl, /home/benj/public_html/declaration//templates/smarty/visibility.tpl, /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Enter email' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Validate my signature' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'%s signatures out of a %d goal' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'Latest signatories' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'See all signatures' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatories' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/index.tpl
	'signatory' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/visibility.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to change your signature visibility (either set it public or private).' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'One last step to confirm your signature!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'You have signed the declaration and we thank you a lot for your support!' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/confirm.tpl
	'A confirmation mail has been sent to <tt>%s</tt>, please follow the confirmation link inside in order to confirm your signature.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'You have signed the \'<b>%s</b>\' declaration and want to cancel your signature.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel.tpl
	'Please fill in your email address in order to authenticate your request.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation confirmation' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Cancellation confirmation has been sent to email <tt>%s</tt>.' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-confirm.tpl
	'Signature cancellation' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation done' => '',

	// In files /home/benj/public_html/declaration//templates/smarty/cancel-validate.tpl
	'Signature cancelation has been confirmed.' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Dear %s' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'you receive this email as you just signed the %s declaration.  We require that you confirm your signature in order to avoid false signatures.' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl
	'To validate your signature, click on the following link :' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-signature.tpl, /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'Note: some email programs would split the preceding URL in two lines.' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl
	'you receive this email as you asked to cancel your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-cancel.tpl, /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'To validate your request, click on the following link :' => '',

	// In files /home/benj/public_html/declaration//templates/mail/petition-visibility.tpl
	'you receive this email as you asked to change visibility for your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.' => '',

	// In files /home/benj/public_html/declaration//./include/utils.php, /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php, /home/benj/public_html/declaration//include/utils.php
	'PETITION_NAME' => 'My first petition',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to find the signature and thus to validate it.' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to send mail to email <tt>%s</tt>' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Missing field "%s"' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Invalid mail !' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'You already have signed the declaration!' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'If you want to receive a new validation mail, <a href="%s">click here</a>.' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature confirmation for the %s declaration' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to validate your signature!' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Signature cancellation for the %s declaration' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to cancel signature, wrong key.' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Visibility changed' => '',

	// In files /home/benj/public_html/declaration//./include/petition.php, /home/benj/public_html/declaration//include/petition.php
	'Unable to change visibility for signature, wrong key.' => '',

	);

?>
