{lang var=`$firstname`}Dear %s{/lang},

{textformat style="mail"}{lang var=$petition_name}you receive this email as you just signed the %s declaration.  We require that you confirm your signature in order to avoid false signatures.{/lang}


{lang}To validate your signature, click on the following link :{/lang}{/textformat}


  {$petition_url}confirm/{$key}


{textformat style="mail"}{lang}Note: some email programs would split the preceding URL in two lines.{/lang}{/textformat}

