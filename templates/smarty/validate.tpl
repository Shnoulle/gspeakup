{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1>{lang}Thanks for your signature and your support!{/lang}</h1>

<p>{lang}Adding your name to the list means that we show to the world how many of us there are. Don't hesitate to spread the word!{/lang}  
{lang}Do not hesitate to ask your friends and relative to sign this declaration or to share your signature using social networks.{/lang}
</p>

<h2>Inscrivez vous à la liste de discussion <em>educ@april.org</em></h2>

<p>Cette liste de travail de l'April sert à coordonner les discussions
  autour du logiciel libre dans l'éducation.  Après confirmation de
  votre demande d'inscription, vous recevrez les mails à destination
  de <em>educ@april.org</em> et vous pourrez poster sur cette liste de
  discussion.</p>

<form class="form-horizontal" action="https://listes.april.org/wws" target="_blank" method="POST">
  <div class="input-group">
    <div class="col-sm-9">
      <input type="email" class="form-control" value="{$signature.email}" placeholder="Mon courriel" name="email"></div>
    <div class="col-sm-3">
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
    </div>
  </div>
  <input type="hidden" name="list" value="educ" />
  <input type="hidden" name="action" value="subrequest" />
  <input type="hidden" name="action_subrequest" value="valider" />
</form>


<h2>Inscrivez vous à la lettre d'information mensuelle de l'April (liste april-actu@)</h2>

<p>Cette liste de diffusion est utilisée par l'April pour envoyer sa lettre d'information mensuelle ainsi que quelques rares annonces liées à l'association. Cette liste est utilisée pour communiquer vers l'extérieur et est modérée (seules quelques personnes peuvent poster).</p> 

<form class="form-horizontal" action="https://listes.april.org/wws" target="_blank" method="POST">
  <div class="input-group">
    <div class="col-sm-9">
      <input type="email" class="form-control" value="{$signature.email}" placeholder="Mon courriel" name="email"></div>
    <div class="col-sm-3">
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
    </div>
  </div>
  <input type="hidden" name="list" value="april-actu" />
  <input type="hidden" name="action" value="subrequest" />
  <input type="hidden" name="action_subrequest" value="valider" />
</form>

<h2>{lang}Share your signature!{/lang}</h2>

{capture name=text}
{lang var=$petition_name,$gpt_base_url}I have signed %s!  What about YOU?  %s{/lang}
{/capture}

<div class="col-md-6">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <div class="panel-title" style="font-size:x-large"><img src="/images/identica.png" alt="Pump.io" />&nbsp;{lang}Share on identi.ca/pump.io{/lang}</div>
    </div>

    <div class="panel-body">
      <p>{lang}Text suggestion{/lang}:</p>
      <form action="http://identi.ca/">
        <textarea class="form-control" >{$smarty.capture.text}</textarea>
	<input class="btn btn-primary" type="submit" value="{lang}Share!{/lang}">
      </form>
    </div>
  </div>
</div>

<div class="col-md-6">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <div class="panel-title" style="font-size:x-large"><img src="/images/tcheep.png" alt="Twitter" />&nbsp;{lang}Share on twitter{/lang}</div>
    </div>
    
    <div class="panel-body">
      <p>{lang}We nevertheless discourage the use of non-free Twitter in favor of the use of Pump.io. <a href='https://www.fsf.org/twitter'>Read more</a> about the issues regarding Twitter on the Free Software Foundation website.{/lang}</p>
      <form method="GET" action="https://twitter.com/intent/tweet">
	<textarea class="form-control" name="text">{$smarty.capture.text}</textarea>
	<input class="btn btn-primary" type="submit" value="{lang}Share!{/lang}" />
      </form>
    </div>
  </div>
</div>

<p style="clear:both"><a href="{$petition_url}">{lang}Return to declaration page{/lang}</a></p>

{include file="footer.tpl"}
