{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<form class="form-horizontal" id="visibility-mail" action="{$petition_url}/visibility" method="POST">
  <h1>{lang}Change signature visibility{/lang}</h1>
  <p>{lang var=`$petition_name`}You have signed the '<b>%s</b>' declaration and want to change your signature visibility (either set it public or private).{/lang}</p>
  <div class="form-group">
    <div class="col-sm-5">
      <input type="email" class="form-control" name="email" placeholder="{lang}Enter email{/lang}" />
    </div>
    <input type="submit" class="btn btn-primary" value="{lang}Change signature visibility{/lang}"/>
  </div>
  ... {lang}or{/lang} <a href="{$petition_url}">{lang}return to declaration page{/lang}</a>
</form>

{include file="footer.tpl"}
