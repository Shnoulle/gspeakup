{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1>{lang}Signature cancelation done{/lang}</h1>

<p>{lang}Signature cancelation has been confirmed.{/lang}</p>

<a href="{$petition_url}">{lang}Return to declaration page{/lang}

{include file="footer.tpl"}
