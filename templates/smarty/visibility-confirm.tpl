{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

{if $email}
<h1>{lang}Signature visibility change confirmation{/lang}</h1>

<p>{lang var=`$email`}A signature visibility change confirmation has been sent to email <tt>%s</tt>.{/lang}</p>
<p>{lang var=`$gpt_mail_bot`}If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.{/lang}</p>
{else}
<h1>{lang}Signature visibility change{/lang}</h1>
<div class="error"><p>{lang var=`$email`}Unable to find email <tt>%s</tt> in database.{/lang}</p>
<p>{lang}If you think this is an issue, please feel to contact us by email using the contact address.{/lang}</p></div>
{/if}

<p><a href="{$petition_url}">{lang}Return to declaration page{/lang}</a></p>

{include file="footer.tpl"}
