{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1 class="post-title text-danger">{lang}Error{/lang} !</h1>

{if $croak}
<div class="alert alert-danger"><p>{$croak}</p></div>
{/if}

{if $croak_notice}
<div class="notice">{$croak_notice}</div>
{/if}

<p>{lang var=$gpt_mail_bot}If you feel this is an error, please do not hesitate to contact us by email at <i>%s</i>.{/lang}</p>

<p><a href="{$petition_url}">{lang}Return to declaration page{/lang}</a></p>

{include file="footer.tpl"}

