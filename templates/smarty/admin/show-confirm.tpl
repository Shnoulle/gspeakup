{include file="header.tpl"}

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Show signature
      <small>Confirmation</small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Show signature
      </li>
    </ol>

    <p>Showing of signature of <b>{$signature.firstname} {$signature.name}</b> achieved !</p>
    <p><a href="{$gpt_base_url}/admin/hide/{$signature.signature_id}" class="btn btn-warning">Hide signature</a></p>
  </div>
</div>

{include file="footer.tpl"}

</body>

</html>
