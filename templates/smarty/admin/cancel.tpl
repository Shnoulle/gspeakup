{include file="header.tpl"}

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Cancel signature
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Cancel signature
      </li>
    </ol>

    <p>Do you with to cancel signature of <b>{$signature.firstname} {$signature.name}</b> ?</p>
    <p><a href="{$gpt_base_url}/admin/cancel-confirm/{$signature.signature_id}" class="btn btn-danger">Yes, cancel signature!</a></p>
  </div>
</div>

{include file="footer.tpl"}

</body>

</html>
