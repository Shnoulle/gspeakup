<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie67 ie678" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="ie8 ie678" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]> <!--><html lang="en"> <!--<![endif]-->
  <head>
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <title>{lang}{$title}{/lang}</title>
    <meta charset="UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="{$gpt_base_url}/images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{$gpt_base_url}/css/chosen.min.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/petition.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/jquery-ui.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/bootstrap-theme.{$gpt_theme}.min.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/flags16.css" />
    {if $petition_map}
    <link rel="stylesheet" href="{$gpt_base_url}/js/jqvmap/jqvmap.css" />
    {/if}
    <!--[if IE]>
	{literal}
	<style type="text/css">
	  .container, .container-fluid
	  {
	  display:table;
	  width: 100%;
	  }
	  .row
	  {
	  display: table-row;
	  }
	  .col-sm-4, .col-sm-6, .col-md-4, .col-md-6
	  {
	  display: table-cell;
	  }
	</style>
	{/literal}
	<![endif]-->

    <script type="text/javascript" src="{$gpt_base_url}/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/chosen.jquery.min.js"></script>
    {if $petition_map}
    <script type="text/javascript" src="{$gpt_base_url}/js/jqvmap/jquery.vmap.min.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/jqvmap/maps/jquery.vmap.{$petition_map}.js"></script>
    {/if}
    <script type="text/javascript" src="{$gpt_base_url}/js/jquery-ui-1.custom.min.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/bootstrap.min.js"></script>


    <style type="text/css">
{literal}
      .panel-heading {
      padding: 5px 15px;
      }

      .panel-footer {
      padding: 1px 15px;
      color: #A0A0A0;
      }

      .profile-img {
      width: 96px;
      height: 96px;
      margin: 0 auto 10px;
      display: block;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      border-radius: 50%;
      }
{/literal}
    </style>

  </head>

  <body role="document" {if $body_id}id="{$body_id}"{/if}>

    <div class="container" style="margin-top:40px">
      <div class="row">
	<div class="col-sm-6 col-md-4 col-md-offset-4">
	  {if $error}<p class="alert alert-danger">{$error}</p>{/if}
	  {if $notice}<p class="alert alert-success">{$notice}</p>{/if}
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <strong> Sign in to continue</strong>
	    </div>
	    <div class="panel-body">
	      <form role="form" action="{$gpt_base_url}/admin/login" method="POST">
		<fieldset>
		  <div class="row">
		    <div class="center-block">
		      <img class="profile-img"
			   src="https://farm6.staticflickr.com/5442/9087583533_3cd0178571_q.jpg" alt="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-sm-12 col-md-10  col-md-offset-1 ">
		      <div class="form-group">
			<div class="input-group">
			  <span class="input-group-addon">
			    <i class="glyphicon glyphicon-user"></i>
			  </span> 
			  <input class="form-control" placeholder="Username" name="user" type="text" autofocus>
			</div>
		      </div>
		      <div class="form-group">
			<div class="input-group">
			  <span class="input-group-addon">
			    <i class="glyphicon glyphicon-lock"></i>
			  </span>
			  <input class="form-control" placeholder="Password" name="password" type="password" value="">
			</div>
		      </div>
		      <div class="form-group">
			<input type="submit" class="btn btn-default btn-primary btn-block" value="Sign in">
		      </div>
		    </div>
		  </div>
		</fieldset>
	      </form>
	    </div>
	  </div>
	  <p class="text-center"><a href="{$gpt_base_url}"><small>Return to petition site.</small></p>
	</div>
      </div>
    </div>

  </body>
</html>
