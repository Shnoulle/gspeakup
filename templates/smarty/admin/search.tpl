{include file="header.tpl"}

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      <i class="glyphicon glyphicon-search"></i> Search {if $query}for «&nbsp;{$query}&nbsp;»{/if}
      <small>{$results_count} result{if $results_count>1}s{/if}</small> 
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Search results
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    {if $results}
    <table class="table table-bordered table-hover">
      <thead>
	<tr>
	  <th>Firstname</th>
	  <th>Name</th>
	  <th>Occupation</th>
	  <th>Email</th>
	  <th>Type</th>
	  <th class="date-euro">Date</th>
	  <th>Ack</th>
	  <th>Actions</th>
      </thead>
      <tbody>
	{section name=r loop=$results}
	<tr>
{if $results[r].signature_type == 0}
	  <td>{$results[r].firstname|escape}</td>
	  <td>{$results[r].name|escape}</td>
	  <td>{$results[r].occupation|escape}</td>
{else}
	  <td colspan="3">{if $results[r].organization_website}<a href="{$results[r].organization_website}">{/if}{$results[r].organization_name|escape}{if $results[r].organization_website}</a>{/if}</td>
	  <td style="display:none !important;"></td>
	  <td style="display:none !important;"></td>
{/if}
	  <td>{if $results[r].keep_mail}<a href="mailto:{$results[r].email|escape}">{/if}{$results[r].email|escape}{if $results[r].keep_mail}</a>{/if}</td>
	  <td>{if $results[r].signature_type==0}Indiv.{else}Orga{/if}</td>
	  <td>{$results[r].signed_time|date_format:'%d/%m/%Y %H:%M'}</td>
	  <td>{if $results[r].validated_time}<i class="glyphicon glyphicon-ok-sign text-success"><span class="hidden">Yes</span></i>{else}<i class="glyphicon glyphicon-question-sign text-warning"><span class="hidden">No</span></i>{/if}</td>
	  <td>
<a href="{$gpt_base_url}/admin/cancel/{$results[r].signature_id}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a> 
{if $results[r].show_signature}
	  <a href="{$gpt_base_url}/admin/hide/{$results[r].signature_id}" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-eye-close"></span> Hide</a>
{else}
	  <a href="{$gpt_base_url}/admin/hide/{$results[r].signature_id}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-eye-open"></span> Show</a>
{/if}
</td>
	  {/section}
    </table>
    {else} 
    <p>No result found...</p>
    {/if}
  </div>
</div>

{include file="footer.tpl"}

<script src="../js/jquery.dataTables.js"></script>
<script src="../js/dataTables.bootstrap.js"></script>
<script>
  {literal}
  $(document).ready(function() {
  $('table').dataTable(
{
  aoColumnDefs: {
        target: 'date-eu',
        sType: "date-eu"
    }
}
);


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;
 
        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split('');
            var frTimea = frDatea[1].split(':');
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }
 
        return x;
    },
 
    "date-euro-asc": function ( a, b ) {
        return a - b;
    },
 
    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
} );


  });

  {/literal}
</script>


</body>

</html>
