{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1>{lang}Signature visibility change{/lang}</h1>

<p>{lang}Your signature is currently:{/lang}
<b>{if $signature.show_signature}{lang}public{/lang}{else}{lang}private{/lang}{/if}</b>
</p>
<form method="POST">
<input type="hidden" name="visibility" value="{if $signature.show_signature}0{else}1{/if}" />
<input type="submit" class="btn btn-primary" 
value="{if $signature.show_signature}{lang}Set my signature to private{/lang}{else}{lang}Set my signature to public{/lang}{/if}{lang}{/lang}">
... {lang}or{/lang} <a href="{$petition_url}">{lang}return to declaration page{/lang}</a>
</form>

{include file="footer.tpl"}
