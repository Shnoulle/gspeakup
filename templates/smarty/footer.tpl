
	</div> <!-- container -->
	</div> <!-- container -->
      </div>   <!-- content-wrapper -->
      
      <!-- Start Footer Wrapper -->

      <!-- End Footer Wrapper -->

    </div>   <!-- page -->

    <footer style="margin-bottom: 0px;">
      <div class="footer navbar navbar-default" style="margin-bottom: 0px;">
	<div class="container-fluid" style="margin-bottom: 0px;">
	  <div class="collapse navbar-collapse navbar-right">
	    <ul class="nav navbar-nav">
	      <li><a href="{$gpt_base_url}/page/About">About</a></li>
	      <li class="dropdown visible-xs">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">{lang}My signature{/lang}&nbsp;<span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="{$gpt_base_url}/visibility"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;{lang}Change signature visibility{/lang}</a></li>
		  <li><a href="{$gpt_base_url}/cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;{lang}Cancel my signature{/lang}</a></li>
		</ul>
	      </li>
              <li><a href="http://gna.org/projects/gspeakup">Powered by gSpeakUp</a></li>
	    </ul>
	  </div>
	</div>
      </div>
    </footer>
  </body>
</html>
