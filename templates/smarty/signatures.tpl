{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1>{lang}Signatures of the declaration{/lang}</h1>

<div class="btn-group" role="group" aria-label="...">
<a class="btn btn-sm btn-default {if $action=='signatures'}active{/if}" href="{$petition_url}signatures/">{lang}See all{/lang}</a>
<a class="btn btn-sm btn-default {if $action=='signatures-individuals'}active{/if}" href="{$petition_url}signatures-individuals/">{lang}See individuals{/lang}</a>
<a class="btn btn-sm btn-default {if $action=='signatures-organizations'}active{/if}" href="{$petition_url}signatures-organizations/">{lang}See organizations{/lang}</a>
</div>

<div id="all-signatures">
  <ul id="signatures-menu">
    <li {if !$country}id="selected"{/if}><a href="{$petition_url}/signatures ">All</a></li>
    <li {if $country=='at'}id="selected"{/if}><a href="{$petition_url}/{$action}/at" title="{lang}at{/lang}">AT</a><li>
    <li {if $country=='be'}id="selected"{/if}><a href="{$petition_url}/{$action}/be" title="{lang}be{/lang}">BE</a><li>
    <li {if $country=='bg'}id="selected"{/if}><a href="{$petition_url}/{$action}/bg" title="{lang}bg{/lang}">BG</a><li>
    <li {if $country=='cy'}id="selected"{/if}><a href="{$petition_url}/{$action}/cy" title="{lang}cy{/lang}">CY</a><li>
    <li {if $country=='cz'}id="selected"{/if}><a href="{$petition_url}/{$action}/cz" title="{lang}cz{/lang}">CZ</a><li>
    <li {if $country=='de'}id="selected"{/if}><a href="{$petition_url}/{$action}/de" title="{lang}de{/lang}">DE</a><li>
    <li {if $country=='dk'}id="selected"{/if}><a href="{$petition_url}/{$action}/dk" title="{lang}dk{/lang}">DK</a><li>
    <li {if $country=='ee'}id="selected"{/if}><a href="{$petition_url}/{$action}/ee" title="{lang}ee{/lang}">EE</a><li>
    <li {if $country=='es'}id="selected"{/if}><a href="{$petition_url}/{$action}/es" title="{lang}es{/lang}">ES</a><li>
    <li {if $country=='fi'}id="selected"{/if}><a href="{$petition_url}/{$action}/fi" title="{lang}fi{/lang}">FI</a><li>
    <li {if $country=='fr'}id="selected"{/if}><a href="{$petition_url}/{$action}/fr" title="{lang}fr{/lang}">FR</a><li>
    <li {if $country=='gb'}id="selected"{/if}><a href="{$petition_url}/{$action}/gb" title="{lang}gb{/lang}">GB</a><li>
    <li {if $country=='gr'}id="selected"{/if}><a href="{$petition_url}/{$action}/gr" title="{lang}gr{/lang}">GR</a><li>
    <li {if $country=='hr'}id="selected"{/if}><a href="{$petition_url}/{$action}/hr" title="{lang}hr{/lang}">HR</a><li>
    <li {if $country=='hu'}id="selected"{/if}><a href="{$petition_url}/{$action}/hu" title="{lang}hu{/lang}">HU</a><li>
    <li {if $country=='ie'}id="selected"{/if}><a href="{$petition_url}/{$action}/ie" title="{lang}ie{/lang}">IE</a><li>
    <li {if $country=='it'}id="selected"{/if}><a href="{$petition_url}/{$action}/it" title="{lang}it{/lang}">IT</a><li>
    <li {if $country=='lt'}id="selected"{/if}><a href="{$petition_url}/{$action}/lt" title="{lang}lt{/lang}">LT</a><li>
    <li {if $country=='lu'}id="selected"{/if}><a href="{$petition_url}/{$action}/lu" title="{lang}lu{/lang}">LU</a><li>
    <li {if $country=='lv'}id="selected"{/if}><a href="{$petition_url}/{$action}/lv" title="{lang}lv{/lang}">LV</a><li>
    <li {if $country=='mt'}id="selected"{/if}><a href="{$petition_url}/{$action}/mt" title="{lang}mt{/lang}">MT</a><li>
    <li {if $country=='nl'}id="selected"{/if}><a href="{$petition_url}/{$action}/nl" title="{lang}nl{/lang}">NL</a><li>
    <li {if $country=='pl'}id="selected"{/if}><a href="{$petition_url}/{$action}/pl" title="{lang}pl{/lang}">PL</a><li>
    <li {if $country=='pt'}id="selected"{/if}><a href="{$petition_url}/{$action}/pt" title="{lang}pt{/lang}">PT</a><li>
    <li {if $country=='ro'}id="selected"{/if}><a href="{$petition_url}/{$action}/ro" title="{lang}ro{/lang}">RO</a><li>
    <li {if $country=='se'}id="selected"{/if}><a href="{$petition_url}/{$action}/se" title="{lang}se{/lang}">SE</a><li>
    <li {if $country=='si'}id="selected"{/if}><a href="{$petition_url}/{$action}/si" title="{lang}si{/lang}">SI</a><li>
    <li {if $country=='sk'}id="selected"{/if}><a href="{$petition_url}/{$action}/sk" title="{lang}sk{/lang}">SK</a><li>
  </ul>

  <div id="signature-list" class="clearfix">

    <ul id="alphabet" class="clearfix">
    <li><a href="{$petition_url}/{$action}/{$country}/a" hreflang="{$country}">A</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/b" hreflang="{$country}">B</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/c" hreflang="{$country}">C</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/d" hreflang="{$country}">D</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/e" hreflang="{$country}">E</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/f" hreflang="{$country}">F</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/g" hreflang="{$country}">G</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/h" hreflang="{$country}">H</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/i" hreflang="{$country}">I</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/j" hreflang="{$country}">J</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/k" hreflang="{$country}">K</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/l" hreflang="{$country}">L</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/m" hreflang="{$country}">M</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/n" hreflang="{$country}">N</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/o" hreflang="{$country}">O</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/p" hreflang="{$country}">P</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/q" hreflang="{$country}">Q</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/r" hreflang="{$country}">R</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/s" hreflang="{$country}">S</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/t" hreflang="{$country}">T</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/u" hreflang="{$country}">U</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/v" hreflang="{$country}">V</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/w" hreflang="{$country}">W</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/x" hreflang="{$country}">X</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/y" hreflang="{$country}">Y</a></li>
    <li><a href="{$petition_url}/{$action}/{$country}/z" hreflang="{$country}">Z</a></li>
    </ul>

    <ul class="signature-list clearfix">
{section name=sign loop=$signatures}
	<li class="row"><div class="col col-md-4 col-sm-2 col-xs-11">{php}
	    $time_ago = explode ( ':',
	    $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['time_ago']);
	    if ( $time_ago [ 0 ] >= 48 )
	    {
	    print strftime(lang('%%Y-%%m-%%d'),$this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['epoch']);
	    }
	    else if ( $time_ago [ 0 ] >= 24 )
	    {
	    print ( lang ( 'Yesterday' ) );
	    }
	    else if ( $time_ago [ 0 ] > 1 )
	    {
	    print ( lang ( "%d hours ago", $time_ago [ 0 ] ) );
	    }
	    else if ( $time_ago [ 0 ] >= 1 )
	    {
	    print ( lang ( "1 hour ago" ) );
	    }
	    else if ( $time_ago [ 1 ] >= 1 )
	    {
	    print ( lang ( "%d minutes ago", $time_ago [ 1 ] ) );
	    }
	    else
	    {
	    print ( lang ( "%d seconds ago", $time_ago [ 2 ] ) );
	    }
	    {/php}
	  </div>
	  <div class="col col-md-1 col-sm-1 col-xs-1">
	    {if $signatures[sign].country}
	    <img width="32" height="32" src="{$petition_url}images/blank.png" class="flag flag-{$signatures[sign].country}" alt="Country" /> 
	    {else}
	    <img src="{$petition_url}images/unknown.png" alt="Country" /> 
	    {/if}
	  </div>
	  
	  <div class="col col-md-7 col-sm-9 col-xs-10">
	    {assign var=country value=$signatures[sign].country}
	    {if $signatures[sign].show_signature}
	    {if $signatures[sign].signature_type == 1}
	    <u>{$signatures[sign].organization_name}</u>{else}
	    <span data-toggle="tooltip" title="{$signatures[sign].occupation}">{$signatures[sign].firstname|escape} {$signatures[sign].name|escape}{/if}</span>{if $signatures[sign].country|escape}, <em>{$country|lang|escape}</em>{/if}
	    {else}
	    {if $signatures[sign].country}{lang var=$country|lang}A signatory from <em>%s</em>{/lang}{else}{lang}A signatory{/lang}{/if}{/if}
	  </div>	
	</li>
{sectionelse}
{if $letter}
<p>{lang}No signature with this letter and country!{/lang}</p>
<p>{lang}How about helping us having more signatures by <a href="/post/Join-Us">spreading the word</a> or signing the declaration yourself, if not done already?{/lang}</p>
{else}
<p>{lang}No signature with this letter and country!{/lang}</p>
{/if}
{/section}
    </ul>

  </div>

    <ul class="pagination clearfix">
    {php}
$count = $this->get_template_vars('signatures_count');
for ( $i = 1 ; ($i-1) <= $count['count'] / 1000 ; $i ++ )
{
  $class='';
  if ( ( array_key_exists ( 'page', $_GET ) && $_GET [ 'page' ] == $i) ||
       ( ! array_key_exists ( 'page', $_GET ) && $i == 1 ) )
    $class=' class="active"';
  print "<li$class><a href=\"?page=$i\">$i</a></li> ";
}
{/php}
    </ul>

</div>

<p><a href="{$petition_url}">{lang}Return to declaration page{/lang}</a></p>

{include file="footer.tpl"}
