<?php /* Smarty version 2.6.20, created on 2015-12-05 23:47:09
         compiled from signatures.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'signatures.tpl', 3, false),array('modifier', 'escape', 'signatures.tpl', 119, false),array('modifier', 'lang', 'signatures.tpl', 119, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array('title' => 'PETITION_TITLE','no_control' => '1')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<h1><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Signatures of the declaration<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h1>

<div class="btn-group" role="group" aria-label="...">
<a class="btn btn-sm btn-default <?php if ($this->_tpl_vars['action'] == 'signatures'): ?>active<?php endif; ?>" href="<?php echo $this->_tpl_vars['petition_url']; ?>
signatures/"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>See all<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a>
<a class="btn btn-sm btn-default <?php if ($this->_tpl_vars['action'] == 'signatures-individuals'): ?>active<?php endif; ?>" href="<?php echo $this->_tpl_vars['petition_url']; ?>
signatures-individuals/"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>See individuals<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a>
<a class="btn btn-sm btn-default <?php if ($this->_tpl_vars['action'] == 'signatures-organizations'): ?>active<?php endif; ?>" href="<?php echo $this->_tpl_vars['petition_url']; ?>
signatures-organizations/"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>See organizations<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a>
</div>

<div id="all-signatures">
  <ul id="signatures-menu">
    <li <?php if (! $this->_tpl_vars['country']): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/signatures ">All</a></li>
    <li <?php if ($this->_tpl_vars['country'] == 'at'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/at" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>at<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">AT</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'be'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/be" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>be<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">BE</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'bg'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/bg" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>bg<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">BG</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'cy'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/cy" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>cy<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">CY</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'cz'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/cz" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>cz<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">CZ</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'de'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/de" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>de<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">DE</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'dk'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/dk" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>dk<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">DK</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'ee'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/ee" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>ee<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">EE</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'es'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/es" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>es<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">ES</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'fi'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/fi" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>fi<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">FI</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'fr'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/fr" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>fr<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">FR</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'gb'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/gb" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>gb<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">GB</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'gr'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/gr" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>gr<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">GR</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'hr'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/hr" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>hr<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">HR</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'hu'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/hu" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>hu<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">HU</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'ie'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/ie" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>ie<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">IE</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'it'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/it" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>it<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">IT</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'lt'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/lt" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>lt<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">LT</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'lu'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/lu" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>lu<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">LU</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'lv'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/lv" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>lv<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">LV</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'mt'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/mt" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>mt<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">MT</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'nl'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/nl" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>nl<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">NL</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'pl'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/pl" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>pl<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">PL</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'pt'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/pt" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>pt<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">PT</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'ro'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/ro" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>ro<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">RO</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'se'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/se" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>se<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">SE</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'si'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/si" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>si<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">SI</a><li>
    <li <?php if ($this->_tpl_vars['country'] == 'sk'): ?>id="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/sk" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>sk<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">SK</a><li>
  </ul>

  <div id="signature-list" class="clearfix">

    <ul id="alphabet" class="clearfix">
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/a" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">A</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/b" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">B</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/c" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">C</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/d" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">D</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/e" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">E</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/f" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">F</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/g" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">G</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/h" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">H</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/i" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">I</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/j" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">J</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/k" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">K</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/l" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">L</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/m" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">M</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/n" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">N</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/o" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">O</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/p" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">P</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/q" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">Q</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/r" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">R</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/s" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">S</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/t" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">T</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/u" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">U</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/v" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">V</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/w" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">W</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/x" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">X</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/y" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">Y</a></li>
    <li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
/<?php echo $this->_tpl_vars['action']; ?>
/<?php echo $this->_tpl_vars['country']; ?>
/z" hreflang="<?php echo $this->_tpl_vars['country']; ?>
">Z</a></li>
    </ul>

    <ul class="signature-list clearfix">
<?php unset($this->_sections['sign']);
$this->_sections['sign']['name'] = 'sign';
$this->_sections['sign']['loop'] = is_array($_loop=$this->_tpl_vars['signatures']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sign']['show'] = true;
$this->_sections['sign']['max'] = $this->_sections['sign']['loop'];
$this->_sections['sign']['step'] = 1;
$this->_sections['sign']['start'] = $this->_sections['sign']['step'] > 0 ? 0 : $this->_sections['sign']['loop']-1;
if ($this->_sections['sign']['show']) {
    $this->_sections['sign']['total'] = $this->_sections['sign']['loop'];
    if ($this->_sections['sign']['total'] == 0)
        $this->_sections['sign']['show'] = false;
} else
    $this->_sections['sign']['total'] = 0;
if ($this->_sections['sign']['show']):

            for ($this->_sections['sign']['index'] = $this->_sections['sign']['start'], $this->_sections['sign']['iteration'] = 1;
                 $this->_sections['sign']['iteration'] <= $this->_sections['sign']['total'];
                 $this->_sections['sign']['index'] += $this->_sections['sign']['step'], $this->_sections['sign']['iteration']++):
$this->_sections['sign']['rownum'] = $this->_sections['sign']['iteration'];
$this->_sections['sign']['index_prev'] = $this->_sections['sign']['index'] - $this->_sections['sign']['step'];
$this->_sections['sign']['index_next'] = $this->_sections['sign']['index'] + $this->_sections['sign']['step'];
$this->_sections['sign']['first']      = ($this->_sections['sign']['iteration'] == 1);
$this->_sections['sign']['last']       = ($this->_sections['sign']['iteration'] == $this->_sections['sign']['total']);
?>
	<li class="row"><div class="col col-md-4 col-sm-2 col-xs-11"><?php 
	    $time_ago = explode ( ':',
	    $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['time_ago']);
	    if ( $time_ago [ 0 ] >= 48 )
	    {
	    print strftime(lang('%%Y-%%m-%%d'),$this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['epoch']);
	    }
	    else if ( $time_ago [ 0 ] >= 24 )
	    {
	    print ( lang ( 'Yesterday' ) );
	    }
	    else if ( $time_ago [ 0 ] > 1 )
	    {
	    print ( lang ( "%d hours ago", $time_ago [ 0 ] ) );
	    }
	    else if ( $time_ago [ 0 ] >= 1 )
	    {
	    print ( lang ( "1 hour ago" ) );
	    }
	    else if ( $time_ago [ 1 ] >= 1 )
	    {
	    print ( lang ( "%d minutes ago", $time_ago [ 1 ] ) );
	    }
	    else
	    {
	    print ( lang ( "%d seconds ago", $time_ago [ 2 ] ) );
	    }
	     ?>
	  </div>
	  <div class="col col-md-1 col-sm-1 col-xs-1">
	    <?php if ($this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['country']): ?>
	    <img width="32" height="32" src="<?php echo $this->_tpl_vars['petition_url']; ?>
images/blank.png" class="flag flag-<?php echo $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['country']; ?>
" alt="Country" /> 
	    <?php else: ?>
	    <img src="<?php echo $this->_tpl_vars['petition_url']; ?>
images/unknown.png" alt="Country" /> 
	    <?php endif; ?>
	  </div>
	  
	  <div class="col col-md-7 col-sm-9 col-xs-10">
	    <?php $this->assign('country', $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['country']); ?>
	    <?php if ($this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['show_signature']): ?>
	    <?php if ($this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['signature_type'] == 1): ?>
	    <u><?php echo $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['organization_name']; ?>
</u><?php else: ?>
	    <span data-toggle="tooltip" title="<?php echo $this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['occupation']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['firstname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<?php endif; ?></span><?php if (((is_array($_tmp=$this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['country'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp))): ?>, <em><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['country'])) ? $this->_run_mod_handler('lang', true, $_tmp) : lang($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</em><?php endif; ?>
	    <?php else: ?>
	    <?php if ($this->_tpl_vars['signatures'][$this->_sections['sign']['index']]['country']): ?><?php $this->_tag_stack[] = array('lang', array('var' => ((is_array($_tmp=$this->_tpl_vars['country'])) ? $this->_run_mod_handler('lang', true, $_tmp) : lang($_tmp)))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>A signatory from <em>%s</em><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?><?php else: ?><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>A signatory<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?><?php endif; ?><?php endif; ?>
	  </div>	
	</li>
<?php endfor; else: ?>
<?php if ($this->_tpl_vars['letter']): ?>
<p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>No signature with this letter and country!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>How about helping us having more signatures by <a href="/post/Join-Us">spreading the word</a> or signing the declaration yourself, if not done already?<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<?php else: ?>
<p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>No signature with this letter and country!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<?php endif; ?>
<?php endif; ?>
    </ul>

  </div>

    <ul class="pagination clearfix">
    <?php 
$count = $this->get_template_vars('signatures_count');
for ( $i = 1 ; ($i-1) <= $count['count'] / 1000 ; $i ++ )
{
  $class='';
  if ( ( array_key_exists ( 'page', $_GET ) && $_GET [ 'page' ] == $i) ||
       ( ! array_key_exists ( 'page', $_GET ) && $i == 1 ) )
    $class=' class="active"';
  print "<li$class><a href=\"?page=$i\">$i</a></li> ";
}
 ?>
    </ul>

</div>

<p><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Return to declaration page<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></p>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>