<?php /* Smarty version 2.6.20, created on 2015-12-03 08:30:55
         compiled from login.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'login.tpl', 8, false),)), $this); ?>
<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie67 ie678" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="ie8 ie678" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]> <!--><html lang="en"> <!--<![endif]-->
  <head>
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <title><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?><?php echo $this->_tpl_vars['title']; ?>
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></title>
    <meta charset="UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/petition.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/bootstrap-theme.<?php echo $this->_tpl_vars['gpt_theme']; ?>
.min.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/flags16.css" />
    <?php if ($this->_tpl_vars['petition_map']): ?>
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/jqvmap.css" />
    <?php endif; ?>
    <!--[if IE]>
	<?php echo '
	<style type="text/css">
	  .container, .container-fluid
	  {
	  display:table;
	  width: 100%;
	  }
	  .row
	  {
	  display: table-row;
	  }
	  .col-sm-4, .col-sm-6, .col-md-4, .col-md-6
	  {
	  display: table-cell;
	  }
	</style>
	'; ?>

	<![endif]-->

    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/chosen.jquery.min.js"></script>
    <?php if ($this->_tpl_vars['petition_map']): ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/jquery.vmap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/maps/jquery.vmap.<?php echo $this->_tpl_vars['petition_map']; ?>
.js"></script>
    <?php endif; ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jquery-ui-1.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/bootstrap.min.js"></script>


    <style type="text/css">
<?php echo '
      .panel-heading {
      padding: 5px 15px;
      }

      .panel-footer {
      padding: 1px 15px;
      color: #A0A0A0;
      }

      .profile-img {
      width: 96px;
      height: 96px;
      margin: 0 auto 10px;
      display: block;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      border-radius: 50%;
      }
'; ?>

    </style>

  </head>

  <body role="document" <?php if ($this->_tpl_vars['body_id']): ?>id="<?php echo $this->_tpl_vars['body_id']; ?>
"<?php endif; ?>>

    <div class="container" style="margin-top:40px">
      <div class="row">
	<div class="col-sm-6 col-md-4 col-md-offset-4">
	  <?php if ($this->_tpl_vars['error']): ?><p class="alert alert-danger"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
	  <?php if ($this->_tpl_vars['notice']): ?><p class="alert alert-success"><?php echo $this->_tpl_vars['notice']; ?>
</p><?php endif; ?>
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <strong> Sign in to continue</strong>
	    </div>
	    <div class="panel-body">
	      <form role="form" action="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/login" method="POST">
		<fieldset>
		  <div class="row">
		    <div class="center-block">
		      <img class="profile-img"
			   src="https://farm6.staticflickr.com/5442/9087583533_3cd0178571_q.jpg" alt="">
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-sm-12 col-md-10  col-md-offset-1 ">
		      <div class="form-group">
			<div class="input-group">
			  <span class="input-group-addon">
			    <i class="glyphicon glyphicon-user"></i>
			  </span> 
			  <input class="form-control" placeholder="Username" name="user" type="text" autofocus>
			</div>
		      </div>
		      <div class="form-group">
			<div class="input-group">
			  <span class="input-group-addon">
			    <i class="glyphicon glyphicon-lock"></i>
			  </span>
			  <input class="form-control" placeholder="Password" name="password" type="password" value="">
			</div>
		      </div>
		      <div class="form-group">
			<input type="submit" class="btn btn-default btn-primary btn-block" value="Sign in">
		      </div>
		    </div>
		  </div>
		</fieldset>
	      </form>
	    </div>
	  </div>
	  <p class="text-center"><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
"><small>Return to petition site.</small></p>
	</div>
      </div>
    </div>

  </body>
</html>