<?php // -*- c -*-
/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C)	2010 Benjamin Drieu (bdrieu@april.org)		      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */


function rfc1522encode($str)
{
    $a = qp_encode($str);
    if (strlen($a) === strlen($str)) {
	return $str;
    }
    if (strchr($str, '=') !== FALSE || strchr($str, '_') !== FALSE) {
	return '=?iso-8859-1?B?' . base64_encode($str) . '?=';
    }
    return '=?iso-8859-1?Q?' . qp_encode($str) . '?=';
}


function qp_encode($str)
{
    $a = 0;
    $b = '';
  
    while ($a < strlen($str)) {
	$c = $str[$a];
	if ((ord($c) & 0x80) !== 0) {
	    $b .= sprintf('=%02X', ord($c));
	}
	else if ( $c == '=' ) {
	    $b .= '=3D';
	} else {
	    $b .= $c;
	}
	$a++;
    }
    return $b;
}



function gpt_send_mail ( $to, $subject, $content )
{
    mb_internal_encoding('UTF-8');
  
    $subject = mb_encode_mimeheader ( $subject, 'UTF-8', 'Q' );
  
    $headers = 'From: ' . GPT_MAIL_BOT . "\n" .
	'Content-Type: text/plain; charset=utf-8' . "\n" .
	'Content-Transfer-Encoding: quoted-printable' . "\n" ;

    return mail ( $to, $subject, qp_encode($content), $headers );
}

?>
