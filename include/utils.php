<?php // -*- c++ -*-

  /* ************************************************************************** */
  /*                                                                            */
  /*     Copyright (C)	2010 Benjamin Drieu (bdrieu@april.org)		      */
  /*                                                                            */
  /*  This program is free software; you can redistribute it and/or modify      */
  /*  it under the terms of the GNU General Public License as published by      */
  /*  the Free Software Foundation; either version 2 of the License, or         */
  /*  (at your option) any later version.                                       */
  /*                                                                            */
  /*  This program is distributed in the hope that it will be useful,           */
  /*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
  /*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
  /*  GNU General Public License for more details.                              */
  /*                                                                            */
  /*  You should have received a copy of the GNU General Public License         */
  /*  along with this program; if not, write to the Free Software               */
  /*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
  /*                                                                            */
  /* ************************************************************************** */


global $cnx;


function debug ( $str )
{
    global $debug;

    if ( $debug )
    {
	print "<div class=\"debug\">" . htmlspecialchars($str) . "</div>";
    }
    if ( array_key_exists ( 'DEBUG', $_ENV ) )
    {
	print "$str\n";
    }
}


function debug_var ( $var )
{
    print '<div id="page"><pre>';
    print_r ( $var );
    print '</pre></div>';
}



function init_smarty ( $args = null, $subdir = '' )
{
  global $SECOND_TOUR;
  global $langs;
  global $election;

  $smarty = new Smarty;
  $smarty -> force_compile = 1;

  if ( defined ( 'TEMPLATE_DIR' ) )
    {
      if ( ! $subdir )
	{
	  $smarty->template_dir = TEMPLATE_DIR . '/en/';
	}
      else
	{
	  $smarty->template_dir = TEMPLATE_DIR . '/' . $subdir . '/';
	}
    }

  if ( defined ( 'COMPILE_DIR' ) )
    {
      $smarty->compile_dir = COMPILE_DIR;
      
      if ( ! is_writable ( COMPILE_DIR ) )
	{
	  @chmod ( COMPILE_DIR, 777 );
	}
      if ( ! is_writable ( COMPILE_DIR ) )
	{
	  die ( lang("Unable to write into %s.",  COMPILE_DIR ) );
	}
    }
        
  if ( @array_key_exists ( 'login', $_SESSION ) and
	   function_exists ( 'est_administrateur' ) and 
	   est_administrateur ( $_SESSION [ 'login' ] ) )
    {
      $smarty -> assign ( 'isadmin', 1 );
    }

  if ( array_key_exists ( 'notice', $_GET ) )
    {
      $smarty -> assign ( 'notice', $_GET [ 'notice' ] );
    }
  if ( array_key_exists ( 'warning', $_GET ) )
    {
      $smarty -> assign ( 'warning', $_GET [ 'warning' ] );
    }
  if ( array_key_exists ( 'error', $_GET ) )
    {
      $smarty -> assign ( 'error', $_GET [ 'error' ] );
    }

  if ( array_key_exists ( 'language', $_SESSION ) )
    $smarty -> assign ( 'language', $_SESSION [ 'language' ] );
  $smarty -> assign ( 'langs', $langs );

  // To capitalize only first letter
  $smarty -> register_modifier ( 'ucfirst', 'smarty_modifier_capitalize_first' );

  // Other
  $smarty -> register_modifier ( 'wiki_format', 'smarty_modifier_wiki_format' );
  $smarty -> register_modifier ( 'attachement_image', 'smarty_modifier_attachement_image' );

  // Do the translation
  $smarty -> register_block ( 'lang', 'do_translation' );
  $smarty -> register_modifier ( 'lang', 'lang' );

  // {translate} is basically the same as lang but will not be catched
  // as a translatable string.
  $smarty -> register_block ( 'translate', 'do_translation' );

  if ( defined ( 'GPT_BASE_URL' ) )
	  {
		  global $_SERVER;
		  if ( ( array_key_exists ( 'HTTP_X_FORWARDED_PROTO', $_SERVER ) && $_SERVER [ 'HTTP_X_FORWARDED_PROTO' ] == 'https' ) ||
			   $_SERVER [ 'SERVER_PORT' ] == 443 )
			  {
				  $smarty -> assign ( 'gpt_base_url', 
									  str_replace ( 'http://', 'https://', GPT_BASE_URL ) );
			  }
		  else
			  $smarty -> assign ( 'gpt_base_url', GPT_BASE_URL );
	  }
  if ( defined ( 'GPT_THEME' ) )
    $smarty -> assign ( 'gpt_theme', GPT_THEME );

  if ( lang('PETITION_NAME') )
	  $smarty -> assign ( 'petition_name', lang ( 'PETITION_NAME' ) );

  $smarty -> assign ( 'subdir', $subdir );

  if ( defined ( 'INKSCAPE_PATH' ) )
    {
      $smarty -> assign ( 'inkscape', 1 );
    }

  return $smarty;
}



/**
 *
 *
 *
 */
function smarty_modifier_capitalize_first ( $string )
{
    return ucfirst ( $string );
}



/**
 * Return a string formated from GPT's pseudo wiki syntax.
 *
 * \param $string	Initiatial string in wiki format.
 *
 * \return		HTML version of input string.
 */
function smarty_modifier_wiki_format ( $string )
{
    $string = htmlspecialchars ( $string );
    $string = preg_replace ( '/(http:\/\/[^\n ]*)/im', 
			     '<a href="$1">$1</a>', 
			     $string );
    $string = preg_replace ( '/^ *- +/im', 
			     '&nbsp;<img alt="*" src="'.GPT_BASE_URL.'/images/triangle.png" />&nbsp;', 
			     $string );

    return $string;

}



/**
 * Obtain an image name from a mime-type.
 *
 * \param $string	A MIME-type
 *
 * \return		Image name.
 */
function smarty_modifier_attachement_image ( $string )
{
    $filename = attachement_get_mime ( $string );

    if ( ! $filename ) 
	$filename = 'unknown';

    return $filename . '.png';
    
}



function multiple_query($q, $link)
{
    $tok = strtok ( $q, ";;\n" );
    while ( $tok )
    {
	$results = mysql_query ( "$tok", $link );
	$tok = strtok ( ";;\n" );
    }
    return $results;
}






function real_do_query ( $query, $host, $user, $passwd, $database )
{
    global $debug;
    global $cnx;
    
    if ( ! $cnx )
    {
	$cnx = mysql_pconnect ( $host, $user, $passwd ) 
	    or die (lang("Unable to connect to database."));
    }
    mysql_select_db ( $database, $cnx ) 
	or die (lang("Unable to connect to database."));

    debug ( $query );
    mysql_query ( "set names 'utf8';", $cnx );
    
    $result = mysql_query ( $query, $cnx );
    if ( ! $result )
    {
	fatal_sql_error ( $cnx, $query );
    }

    if ( preg_match ( "/^[\( ]*select/i", $query ) ||
	 preg_match ( "/^[\( ]*show/i", $query ) ||
	 preg_match ( "/^[\( ]*describe/i", $query ) )
    {
	return $result;
    }
    else if ( preg_match ( "/^[\( ]*update/i", $query ) )
    {
	$_kaBoom=explode(' ',mysql_info());
	return $_kaBoom[2];
    }
    else
    {
	return mysql_affected_rows ( $cnx ) ;
    }
}



function do_query ( $query )
{
    global $debug, $host, $user, $passwd, $database;

    $result = real_do_query ( $query, $host, $user, $passwd, $database);

    return $result;
}



/**
 * Simply do a query and return data associated if applicable (for
 * select, show and describe queries).
 *
 * \param $query	SQL query to execute.
 *
 * \return		Data returned by query if applicable, result
 *			otherwise.
 */ 
function simple_query ( $query )
{
    $data = Array ( );
    $result = do_query ( $query );
    if ( preg_match ( "/^[ \(]*select/i", $query ) ||
	 preg_match ( "/^[ \(]*show/i", $query ) ||
	 preg_match ( "/^[ \(]*describe/i", $query ) )
    {
	$num = mysql_num_rows ( $result );
	for ( $i = 0 ; $i < $num ; $i ++ )
	{
	    $data [ $i ] = mysql_fetch_array ( $result, MYSQL_ASSOC );
	}
	return $data;
    }

    return $result;
}



/**
 * Simply do a query and return data associated if applicable (for
 * select, show and describe queries).  Return only first occurence.
 *
 * \param $query	SQL query to execute.
 *
 * \return		Data returned by query if applicable, result
 *			otherwise.
 */ 
function simple_unique_query ( $query )
{
    $data = simple_query ( $query );
    if ( $data )
	return $data [ 0 ];
}



function arg2sqldate ( $value, $sql_syntax = true )
{
    if ( ! $value || $value == 'NULL' )
    {
	if ( $sql_syntax )
	    return "NULL";
	else
	    return '';
    }    

    // Convert dd/mm/yyyy to yyyy-mm-dd
    if ( preg_match ( '/([0-9]*)\/([0-9]*)\/([0-9]*)/', $value, $matches ) )
    {
	/* That's lame but I could not find another way to say that,
	 * say, 07 == 2007 and 69 == 1969. */
	if ( $matches [ 3 ] != '0000' && $matches [ 3 ] < 15 )
	{
	    $matches [ 3 ] = 2000 + $matches [ 3 ];
	}
	else if ( $matches [ 3 ] != '0000' && $matches [ 3 ] < 100 )
	{
	    $matches [ 3 ] = 1900 + $matches [ 3 ];
	}
	$value = $matches [ 3 ] . '-' . $matches [ 2 ] . '-' . $matches [ 1 ]; 
    }
    
    if ( $sql_syntax )
	return "date('$value')";
    else
	return $value;
}


function sql2date ( $arg )
{
    if ( preg_match ( '/([0-9]*)-([0-9]*)-([0-9]*)/', $arg, $matches ) )
    {
	return $matches[3] . '/' . $matches[2] . '/' . $matches[1]; 
    }

    // Puzzled, safer to return initial value
    return $arg;
}


// TODO: implement cache to speed up this, but I am so lazy.
function find_column_type ( $table, $column )
{
    $result = do_query ('describe ' . $table . ' ' . $column );
    $line = mysql_fetch_array ( $result, MYSQL_ASSOC );

    return $line['Type'];
}



/**
 * Perform a HTTP redirection to a specified URL.  If URL parameter is
 * omitted, redirection will be done to the base URL of GPT.
 *
 */
function redirect ( $location )
{				
    if ( ! $location )
	$location = GPT_BASE_URL;

    header ( "Location: $location" );
}



function redirect_to_previous_with_message ( $suffix )
{
    if ( strchr ( $_SERVER [ 'HTTP_REFERER' ], '?' ) )
	redirect ( $_SERVER [ 'HTTP_REFERER' ] . $suffix );
    else
	redirect ( $_SERVER [ 'HTTP_REFERER' ] . '?' . $suffix );
}



function croak ( $message )
{
    global $smarty;

    if ( ! $smarty )
    {
	die ( $message . "\n" );
	exit;
    } 
    @$smarty -> assign ( 'croak', lang($message) );

    if ( is_readable ( $smarty -> template_dir . '/croak.tpl' ) )
    {
	$smarty -> display ( "croak.tpl" );
    } 
    else
    {
	die ( $message );
    }
  
    exit ( 1 );
}

 
function notice ( $message )
{
    global $smarty;

    @$smarty -> assign ( 'croak_notice', $message );

    if ( is_readable ( $smarty -> template_dir . '/croak.tpl' ) )
    {
	$smarty -> display ( "croak.tpl" );
    } 
    else
    {
	die ( $message );
    }
  
    exit ( 1 );
}

 
function fatal_sql_error ( $dbh = null, $query = null )
{
    global $election;
    global $_GET;
    global $_POST;

    if ( $dbh )
		{
			$error_no = mysql_errno($dbh);
			$error_string = mysql_error($dbh);
		}
    $mail_content = sprintf ( "URL : %s\n" .
							  "Time : %s\n" .
							  "User : %s\n" .
							  "Query : %s\n" .
							  "Error : %s\n" .
							  "Post : %s\n\n" .
							  "Get : %s\n\n" .
							  "Server environment : %s\n",
							  $_SERVER [ 'SCRIPT_NAME' ],
							  strftime('%d/%m/%Y %H:%M:%S'),
							  ( array_key_exists ( 'login', $_SESSION ) ? $_SESSION [ 'login' ] : 'null' ),
							  $query, $error_string,
							  var_export ( $_POST, TRUE ),
							  var_export ( $_GET, TRUE ),
							  var_export ( $_SERVER, TRUE ) );
	if ( ! $_SESSION  )
		{
			printf ( "URL : %s\n" .
					 "Time : %s\n" .
					 "User : %s\n" .
					 "Query : %s\n" .
					 "Error : %s\n",
					 $_SERVER [ 'SCRIPT_NAME' ],
					 strftime('%d/%m/%Y %H:%M:%S'),
					 ( array_key_exists ( 'login', $_SESSION ) ? $_SESSION [ 'login' ] : 'null' ),
					 $query, $error_string );
			exit ( 1 );
		}


    gpt_send_mail ( GPT_WEBMASTER, 'Rapport d\'erreur ' . $election [ 'name' ], $mail_content );
  
    croak ( lang("<p><b>A SQL error has been triggered</b>.  Webmaster has been informed and will take actions soon.</p><p>You may want to reload page or retry later.</p>") .
			( $_SESSION [ 'admin' ] ? 
			  "<pre class=\"pre-formatted\">$query</pre><p><b>$error_string</b>" : '' ) );
}



function unescape_http_args ( $args )
{
    if ( get_magic_quotes_gpc ( ) )
	foreach ( $args as $key => $value )
	{
	    $args [ $key ] = stripslashes ( $value );
	}

    return $args;
}




function set_debug ( $default = 1 )
{
    global $debug;
    $debug = $default;
}






/**
 * Simple Accept-Language handler.  Just split the Accept-Language
 * string and find in the given order if there is a language that
 * matches defined languages.  If not, use 'en' as a default.
 *
 * \param	$var	A string conform to RFC 2616
 *
 * \return	A two digit language code.
 */
function find_browser_language ( $var )
{
    global $langs;

    $browser_langs = preg_split ( '/, */', $var );

    foreach ( $browser_langs as $browser_lang )
    {
	preg_match ( '/^([a-z]*)(?:-?([a-z]*))? *(?:;q=([-0-9\.]*))?$/', 
		     $browser_lang, 
		     $details );
	foreach ( $langs as $lang )
	{
	    if ( $lang [ 'id' ] == $details [ 1 ] ||
		 $lang [ 'id' ] == $details [ 1 ] . '-' . $details [ 2 ] )
	    {
		return $lang [ 'id' ];
	    }
	}
    }
    
    return 'en';
}



function maybe_addslashes ( $string )
{
    if ( get_magic_quotes_gpc ( ) )
	return $string;
    else
	return ( addslashes ( $string ) );
}



function message_assign ( $param, $message )
{
    global $smarty;
    if ( array_key_exists ( $param, $_GET ) )
		$smarty -> assign ( 'message',  lang($message) );
}



function strip_get ( $url )
{
    return preg_replace ( '/\?.*/', '', $url );
}


/* Local Variables: */
/* c-basic-offset: 4 */
/* tab-width: 4 */
/* c-font-lock-extra-types: Worker, Listener, Listener, InmarsatData */
/* End: */
?>
