<?php // -*- c++ -*-

/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C) 2007-2014 Benjamin Drieu (bdrieu@april.org)	      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */


require '../include/options.php';

// Set session to two days
session_cache_expire (2880);
ini_set("session.gc_maxlifetime", "172800"); 
ini_set("session.cookie_lifetime", "172800"); 
ini_set("session.cookie_domain", GPT_DOMAIN ); 
session_set_cookie_params ( 172800 );
session_name ( 'GPTSESSID' );
session_start ();

require '../include/utils.php';
require '../include/language.php';
require '../include/mail.php';
require '../include/petition.php';

require '../lang/default.php';

$info = $_SERVER [ 'PATH_INFO' ];
$info = preg_replace ( '/\/\//', '/', $info );
$info = preg_replace ( '/^\/*/', '', $info );
$path_info = @split ( '/', $info );
$action = $path_info [ 0 ];
$what = @$path_info [ 1 ];
$object = @$path_info [ 2 ];

if ( $action != 'images' )
  header("Content-Type: text/html; charset=utf-8");
 else
   header("Content-Type: image/png");

$smarty = init_smarty ( null, 'admin' );
if ( preg_match ( '/^http:/', GPT_BASE_URL ) )
{
    $smarty -> assign ( 'gpt_base_url', str_replace ( 'http://', 'https://', GPT_BASE_URL ) );
}

$smarty -> assign ( 'action', $action );
$smarty -> assign ( 'what', $what );
$smarty -> assign ( 'object', $object );


if ( $_SERVER [ 'HTTP_X_FORWARDED_PROTO' ] != 'https' &&
     $_SERVER [ 'SERVER_PORT' ] != 443 )
{
    redirect ( str_replace ( 'http://', 'https://', GPT_BASE_URL . 'admin/' ) );
}

if ( $action == 'logout' )
{
    unset ( $_SESSION [ 'admin' ] );
    $smarty -> assign ( 'notice', 'You have been logged out.' );
}

if ( ! array_key_exists ( 'admin', $_SESSION ) || $action == 'login' )
{
    if ( $_POST [ 'user' ] )
    {
	if ( $_POST [ 'user' ] != PETITION_ADMIN_USER || 
	     sha1 ( $_POST [ 'password' ] ) != PETITION_ADMIN_PASSWORD )
	{
	    $smarty -> assign ( 'error', 'Wrong login or username' );
	    return $smarty -> display ( 'login.tpl' );
	}
	else
	{
	    $_SESSION [ 'admin' ] = true;
	    redirect ( GPT_BASE_URL . 'admin/' );
	}
    }
    else
    {
	return $smarty -> display ( 'login.tpl' );
    }
}

if ( $action == 'search' )
{
    $results = simple_query ( sprintf
			      ( "SELECT * FROM signature " .
				" WHERE firstname like '%%%s%%' " .
				"    OR name like '%%%s%%' " .
				"    OR organization_name like '%%%s%%';",
				maybe_addslashes ( $_POST [ 'q' ] ), 
				maybe_addslashes ( $_POST [ 'q' ] ), 
				maybe_addslashes ( $_POST [ 'q' ] ) ) );
    $smarty -> assign ( 'query', $_POST [ 'q' ] );
    $smarty -> assign ( 'results_count', sizeof($results) );
    $smarty -> assign ( 'results', $results );
    $smarty -> display ( 'search.tpl' );
  }
else if ( $action == 'cancel' )
{
    $signature = simple_unique_query ( sprintf
				       ( "SELECT * FROM signature " .
					 " WHERE signature_id = '%d';",
					 maybe_addslashes ( $what ) ) );
    if ( ! $signature )
	return croak ( 'Unable to find signature' );
    $smarty -> assign ( 'signature', $signature );
    $smarty -> display ( 'cancel.tpl' );   
}
else if ( $action == 'cancel-confirm' )
{
    $signature = simple_unique_query ( sprintf
				       ( "SELECT * FROM signature " .
					 " WHERE signature_id = '%d';",
					 maybe_addslashes ( $what ) ) );
    if ( ! $signature )
	return croak ( 'Unable to find signature' );
    $smarty -> assign ( 'signature', $signature );
    simple_unique_query ( sprintf
			  ( "DELETE FROM signature " .
			    " WHERE signature_id = '%d';",
			    maybe_addslashes ( $what ) ) );
    $smarty -> display ( 'cancel-confirm.tpl' );   
}
else if ( $action == 'hide' )
{
    set_debug();
    $signature = simple_unique_query ( sprintf
				       ( "SELECT * FROM signature " .
					 " WHERE signature_id = '%d';",
					 maybe_addslashes ( $what ) ) );
    if ( ! $signature )
	return croak ( 'Unable to find signature' );
    $smarty -> assign ( 'signature', $signature );
    simple_unique_query ( sprintf
			  ( "UPDATE signature " .
			    "   SET show_signature = 0 " .
			    " WHERE signature_id = '%d';",
			    maybe_addslashes ( $what ) ) );
    $smarty -> display ( 'hide-confirm.tpl' );   

}
else if ( $action == 'show' )
{
    set_debug();
    $signature = simple_unique_query ( sprintf
				       ( "SELECT * FROM signature " .
					 " WHERE signature_id = '%d';",
					 maybe_addslashes ( $what ) ) );
    if ( ! $signature )
	return croak ( 'Unable to find signature' );
    $smarty -> assign ( 'signature', $signature );
    simple_unique_query ( sprintf
			  ( "UPDATE signature " .
			    "   SET show_signature = 1 " .
			    " WHERE signature_id = '%d';",
			    maybe_addslashes ( $what ) ) );
    $smarty -> display ( 'show-confirm.tpl' );   

 }
else if ( ! $action )
  {
    $smarty -> assign ( 'signatures_total', simple_query ( "SELECT UNIX_TIMESTAMP(signature.signed_time) AS date, COUNT(1) AS count FROM signature WHERE validated_time IS NOT NULL GROUP BY signed_time ORDER BY signed_time;") );
    $smarty -> assign ( 'signatures', simple_query ( "SELECT UNIX_TIMESTAMP(signature.signed_time) AS date, COUNT(1) AS count FROM signature WHERE validated_time IS NOT NULL GROUP BY YEAR(signed_time), MONTH(signed_time), DAY(signed_time), HOUR(signed_time) ORDER BY signed_time;") );
    $smarty -> assign ( 'signatures_stats', simple_unique_query ( "SELECT UNIX_TIMESTAMP(MIN(signature.signed_time)) AS min_signature, UNIX_TIMESTAMP(MAX(signature.signed_time)) AS max_signature FROM signature WHERE validated_time IS NOT NULL;") );
    $smarty -> assign ( 'signature_type', simple_query  ( "SELECT COUNT(1) AS count FROM signature WHERE validated_time IS NOT NULL GROUP BY signature_type ORDER BY signature_type" ) );
    $smarty -> assign ( 'referers_domains', 
			simple_query ( "SELECT REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(referer,'/',3), '/', -1), 'www.', '') AS domain, COUNT(1) AS count FROM signature WHERE referer IS NOT NULL and referer != '' AND validated_time IS NOT NULL GROUP BY domain ORDER BY count DESC;" ) );
    $referers = simple_query ( "SELECT REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(referer,'/',3), '/', -1), 'www.', '') AS domain, referer, COUNT(1) AS count FROM signature WHERE referer IS NOT NULL and referer != '' AND validated_time IS NOT NULL GROUP BY referer ORDER BY count DESC;" );
    $smarty -> assign ( 'referers', $referers );

    $smarty -> assign ( 'countries', 
			simple_query ( "SELECT country, COUNT(1) AS count FROM signature WHERE validated_time IS NOT NULL GROUP BY country ORDER by count DESC;" ) );
    $smarty -> display ( 'index.tpl' );
}
else
{
     croak ( "Wrong page !" );
}

?>
